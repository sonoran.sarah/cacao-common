// Package service is a temporary package and will eventually be replace
// with cacao-types/service/user.go
// TODO: EJS - replace with cacao-types
package service

import "time"

const (
	DefaultUserListItemsMax = 500 // Default maximum number of UserList items to return

	// ReservedCacaoSystemActor is a special actor that is used by the API gateway and perhaps other internal services,
	// which may be used to perform adminstrative operations on the user microservice, such as autocreating users
	// This is not a real user and logic should be added to prohibit creation of such a username
	ReservedCacaoSystemActor = "RESERVED_CACAO_SYSTEM_ACTOR"
)

type UserClient interface {
	//EventSource // This will be embedded later and there should be a BaseService
	Add(user User) error
	Update(user User) error
	Get(username string) (User, error) // get a user based on username
	Delete(user User) error
	Login(user User) error

	Search(filter UserListFilter) (UserList, error) // Given a filter, return a user list up to DefaultUserListItemsMax
	SearchNext(userlist UserList) error             // load the next in a search, given the original UserList
}

// User is the standard interface for all User implementations
type User interface {
	SessionContext
	GetPrimaryID() string // points to username
	GetUsername() string  // Username is the primary key
	GetFirstName() string
	GetLastName() string
	GetPrimaryEmail() string
	GetIsAdmin() bool
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetUpdatedBy() string
	GetUpdatedEmulatorBy() string
	GetDisabledAt() time.Time

	SetPrimaryID(string)
	SetUsername(string)
	SetFirstName(string)
	SetLastName(string)
	SetPrimaryEmail(string)
	SetIsAdmin(bool)
	SetDisabledAt(time.Time)

	// methods for preferences
	GetPreferencesMap() map[string]string // returns a readonly copy of preferences as a map
	GetPreference(key string) string
	SetPreference(key string, value string)
	IsPreferencesLoaded() bool
}

type UserModel struct {
	Session
	Username             string            `json:"username"`
	FirstName            string            `json:"first_name,omitempty"`
	LastName             string            `json:"last_name,omitempty"`
	PrimaryEmail         string            `json:"primary_email,omitempty"`
	IsAdmin              bool              `json:"is_admin,omitempty"`
	DisabledAt           time.Time         `json:"disabled_at,omitempty"`
	Preferences          map[string]string `json:"preferences"`
	ForceLoadPreferences bool              `json:"force_load_preferences"` // should only be set to true with LoadPreferencesLoadOption
	isPreferencesLoaded  bool              `json:"-"`
	CreatedAt            time.Time         `json:"created_at"`
	UpdatedAt            time.Time         `json:"updated_at"`
	UpdatedBy            string            `json:"updated_by"`
	UpdatedEmulatorBy    string            `json:"updated_emulator_by"`
}

func NewUser(username string) User {
	return &UserModel{
		Username: username,
	}
}

func (u *UserModel) SetPrimaryID(s string) {
	u.SetUsername(s)
}
func (u *UserModel) SetUsername(s string) {
	if u.Username != s {
		u.Username = s
	}
}
func (u *UserModel) SetFirstName(s string) {
	if u.FirstName != s {
		u.FirstName = s
	}
}
func (u *UserModel) SetLastName(s string) {
	if u.LastName != s {
		u.LastName = s
	}
}
func (u *UserModel) SetPrimaryEmail(s string) {
	if u.PrimaryEmail != s {
		u.PrimaryEmail = s
	}
}
func (u *UserModel) SetIsAdmin(b bool) {
	if u.IsAdmin != b {
		u.IsAdmin = b
	}
}
func (u *UserModel) SetDisabledAt(d time.Time) {
	if u.DisabledAt != d {
		u.DisabledAt = d
	}
}

func (u UserModel) GetPrimaryID() string {
	return u.Username
}
func (u UserModel) GetUsername() string {
	return u.Username
}
func (u UserModel) GetFirstName() string {
	return u.FirstName
}
func (u UserModel) GetLastName() string {
	return u.LastName
}
func (u UserModel) GetPrimaryEmail() string {
	return u.PrimaryEmail
}
func (u UserModel) GetIsAdmin() bool {
	return u.IsAdmin
}
func (u UserModel) GetCreatedAt() time.Time {
	return u.CreatedAt
}
func (u UserModel) GetUpdatedAt() time.Time {
	return u.CreatedAt
}
func (u UserModel) GetUpdatedBy() string {
	return u.UpdatedBy
}
func (u UserModel) GetUpdatedEmulatorBy() string {
	return u.UpdatedEmulatorBy
}
func (u UserModel) GetDisabledAt() time.Time {
	return u.DisabledAt
}

// GetPreferencesMap gets a readonly map version of the preferences
func (u UserModel) GetPreferencesMap() map[string]string {
	var newmap map[string]string = nil
	if u.Preferences == nil {
		newmap = make(map[string]string)
		for k, v := range u.Preferences {
			newmap[k] = u.Preferences[v]
		}
	}
	return newmap
}

func (u UserModel) GetPreference(key string) string {
	return u.Preferences[key]
}

func (u *UserModel) SetPreference(key string, value string) {
	if u.Preferences[key] != value {
		u.Preferences[key] = value
	}
}
func (u *UserModel) IsPreferencesLoaded() bool {
	return u.isPreferencesLoaded
}

// UserList is the interface that defines objects that can retrieve lists of users
// TODO - currently not implemented
type UserList interface {
	SessionContext

	GetFilter() UserListFilter
	GetUsers() []User
	GetStart() int           // index of start of current list, -1 if no results or error, available after successful Load()
	GetSize() int            // size of the current list, 0 if nothing is found, -1 if error, available after successful Load()
	GetTotalFilterSize() int // total size of filter, 0 if nothing is found, -1 if error
	GetNextStart() int       // index of next partition to retrieve, -1 if error is found, set to TotalFilterSize if all results returned
}

// UserListFilter is the struct that defines the filter for a UserList
type UserListFilter struct {
	Field           FilterField   `json:"field"`
	Value           string        `json:"value"`                    // if not set, defaults to all
	SortBy          SortDirection `json:"sort_direction,omitempty"` // default is AscendingSort
	MaxItems        int           `json:"max_items,omitempty"`      // requested max items to return in the list, defaults to DefaultUserListItemsMax
	Start           int           `json:"start_index"`              // start index to retrieve from total list, default 0
	IncludeDisabled bool          `json:"include_disabled"`         // if true, then disabled users will also be included in the filter (not yet implemented)
}

type FilterField string

const UsernameFilterField FilterField = "UsernameField"
const IsAdminFilterField FilterField = "IsAdminField"

type SortDirection int

const AscendingSort SortDirection = 1 // This should be the default if omitted
const DescendingSort SortDirection = -1

type UserListModel struct {
	Session
	Filter               UserListFilter
	Users                []User
	StartIndex           int  `json:"start_index"`
	TotalSize            int  `json:"total_size"`
	NextStart            int  `json:"next_start"`
	ForceLoadPreferences bool `json:"force_load_preferences"` // default should be false
}

func (ul *UserListModel) GetFilter() UserListFilter {
	return ul.Filter
}
func (ul *UserListModel) GetUsers() []User {
	return ul.Users
}
func (ul *UserListModel) GetStart() int {
	return ul.StartIndex
}
func (ul *UserListModel) GetSize() int {
	return len(ul.Users)
}
func (ul *UserListModel) GetTotalFilterSize() int {
	return ul.TotalSize
}
func (ul *UserListModel) GetNextStart() int {
	return ul.NextStart
}
func (ul *UserListModel) HasNext() bool {
	return ul.NextStart >= 0
}
