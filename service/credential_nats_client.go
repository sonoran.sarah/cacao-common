package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"net/http"
	"time"
)

const (
	NatsSubjectCredentials     string = common.NatsQueryOpPrefix + "credentials"
	NatsSubjectCredentialsGet         = NatsSubjectCredentials + ".get"
	NatsSubjectCredentialsList        = NatsSubjectCredentials + ".list"

	EventCredentialAddRequested    common.EventType = "org.cyverse.events.CredentialAddRequested"
	EventCredentialUpdateRequested common.EventType = "org.cyverse.events.CredentialUpdateRequested"
	EventCredentialDeleteRequested common.EventType = "org.cyverse.events.CredentialDeleteRequested"

	EventCredentialAdded   common.EventType = "org.cyverse.events.CredentialAdded"
	EventCredentialUpdated common.EventType = "org.cyverse.events.CredentialUpdated"
	EventCredentialDeleted common.EventType = "org.cyverse.events.CredentialDeleted"

	EventCredentialAddError    common.EventType = "org.cyverse.events.CredentialAddError"
	EventCredentialUpdateError common.EventType = "org.cyverse.events.CredentialUpdateError"
	EventCredentialDeleteError common.EventType = "org.cyverse.events.CredentialDeleteError"
	EventCredentialGetError    common.EventType = "org.cyverse.events.CredentialGetError"
	EventCredentialListError   common.EventType = "org.cyverse.events.CredentialListError"
)

type natsCredentialClient struct {
	actor          string
	emulator       string
	clientID       string
	natsUrl        string
	clusterID      string
	natsConfig     messaging.NatsConfig
	stanConfig     messaging.StanConfig
	maxReconnect   int
	reconnectWait  time.Duration
	requestTimeout time.Duration
	eventsChannel  string
	eventsTimeout  time.Duration
	ctx            context.Context
}

func NewNatsCredentialClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (CredentialClient, error) {
	log.Trace("NewNatsCredentialClient() called")

	if actor == "" {
		return nil, errors.New("actor Not Set")
	}

	if ctx == nil {
		ctx = context.Background()
	}
	newCredentialSvc := natsCredentialClient{
		actor:          actor,
		emulator:       emulator,
		natsUrl:        natsConfig.URL,
		clientID:       natsConfig.ClientID,
		clusterID:      stanConfig.ClusterID,
		eventsChannel:  common.EventsSubject,
		natsConfig:     natsConfig,
		stanConfig:     stanConfig,
		requestTimeout: 10,
		ctx:            ctx}

	// for _, option := range options { // call the options
	// 	option(&new_user_svc)
	// }

	if natsConfig.MaxReconnects <= 0 {
		newCredentialSvc.maxReconnect = messaging.DefaultNatsMaxReconnect
	} else {
		newCredentialSvc.maxReconnect = natsConfig.MaxReconnects
	}
	if natsConfig.ReconnectWait <= 0 {
		newCredentialSvc.reconnectWait = messaging.DefaultNatsReconnectWait
	} else {
		newCredentialSvc.reconnectWait = time.Duration(natsConfig.ReconnectWait) * time.Second
	}
	if natsConfig.RequestTimeout <= 0 {
		newCredentialSvc.requestTimeout = messaging.DefaultNatsRequestTimeout
	} else {
		newCredentialSvc.requestTimeout = time.Duration(natsConfig.RequestTimeout) * time.Second
	}

	return &newCredentialSvc, nil
}

func waitCredentialResponse(c chan common.ServiceRequestResult, timeout time.Duration) (bool, common.ServiceRequestResult) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, common.ServiceRequestResult{}
	}
}

func (credsvc *natsCredentialClient) Get(ID string) (Credential, error) {

	log.Trace("natsCredentialClient.Get Called")

	if len(ID) <= 0 {
		return nil, errors.New("invalid credential ID")
	}

	nc, err := nats.Connect(credsvc.natsUrl, nats.MaxReconnects(credsvc.maxReconnect), nats.ReconnectWait(credsvc.reconnectWait))
	if err != nil {
		log.Trace("a1")
		return nil, err
	}
	defer nc.Close()

	credential := CredentialModel{Session: Session{SessionActor: credsvc.actor, SessionEmulator: credsvc.emulator},
		Username: credsvc.actor, ID: ID}

	ce, err := messaging.CreateCloudEvent(credential, NatsSubjectCredentialsGet, credsvc.clientID)

	if err != nil {
		return nil, err
	}

	bytes, err := json.Marshal(ce)

	if err != nil {
		return nil, err
	}

	newctx, cancel := context.WithTimeout(credsvc.ctx, credsvc.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, NatsSubjectCredentialsGet, bytes)
	cancel()

	if err != nil {
		log.Trace("a4")
		return nil, err
	}

	// convert the message to a cloud event
	log.Trace("natsCredsclient.Get: received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(ce.Data(), &credential)
	if err != nil {
		return nil, err
	}

	// final check for error
	if credential.ErrorType != "" {
		log.Trace("natsCredentialClient.Get: error was received:" + credential.ErrorType)
		err = errors.New(credential.ErrorType)
	}

	return &credential, err

}

func (credsvc *natsCredentialClient) Update(credential Credential) (map[string]interface{}, error) {
	//TODO as of 6/3/2021 this just uses the add function, which will update the secret
	//However... it wont delete the old secret for example if a name is changed.
	//Therefore this needs to be updated
	//TODD TODO
	return credsvc.Add(credential)
}

func (credsvc *natsCredentialClient) Add(credential Credential) (map[string]interface{}, error) {
	log.Trace("natsCredentialClient.Add() started")

	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsCredentialClient.Add",
	})

	valid, verr := credential.Verify()
	transactionID := messaging.NewTransactionID()

	if !valid || verr != nil {
		logger.WithFields(log.Fields{"error": verr}).Error("Unable to verify provided Credential")
		message := map[string]interface{}{
			"errMsg":    "Unable to verify provided Credential",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, verr
	}

	SenderNatsConfig := credsvc.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &credsvc.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		message := map[string]interface{}{
			"errMsg":    "Unable to connect to NATS Streaming",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := credsvc.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, credsvc.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		message := map[string]interface{}{
			"errMsg":    "Unable to connect to NATS Streaming",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}

	request := credential

	eventsSubscribe := []common.EventType{
		EventCredentialAdded,
		EventCredentialAddError,
	}

	responseChannel := make(chan common.ServiceRequestResult)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == EventCredentialAdded || ev == EventCredentialAddError {
			var cred CredentialModel
			err := json.Unmarshal(ce.Data(), &cred)
			if err != nil {
				logger.WithError(err).Error("unable to unmarshal a credential add response")
				responseChannel <- common.ServiceRequestResult{
					Data: nil,
					Status: common.HTTPStatus{
						Message: "unable to unmarshal a credential add response",
						Code:    http.StatusInternalServerError,
					},
				}
				return
			}
			errType, errMsg := cred.GetError()
			if errType != "" {
				responseChannel <- common.ServiceRequestResult{
					Data: nil,
					Status: common.HTTPStatus{
						Message: fmt.Sprintf("%s, %s", errType, errMsg),
						Code:    http.StatusInternalServerError, // TODO derive error code from ErrorType
					},
				}
				return
			}
			responseChannel <- common.ServiceRequestResult{
				Data: cred,
				Status: common.HTTPStatus{
					Message: "",
					Code:    http.StatusOK,
				},
			}
			return
		}

		responseChannel <- common.ServiceRequestResult{
			Data: nil,
			Status: common.HTTPStatus{
				Message: fmt.Sprintf("unknown event type %s", ev),
				Code:    http.StatusInternalServerError,
			},
		}
	}
	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithError(err).Error("unable to add an event listener")
		message := map[string]interface{}{
			"errMsg":    "unable to add an event listener",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(EventCredentialAddRequested, request, transactionID)
	if err != nil {
		logger.WithError(err).Error("unable to publish a add credential event")
		message := map[string]interface{}{
			"errMsg":    "unable to publish a add credential event",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}

	responseReceived, response := waitCredentialResponse(responseChannel, credsvc.requestTimeout*time.Second)
	if responseReceived {

		if response.Status.Code == http.StatusAccepted || response.Status.Code == http.StatusOK {
			message := map[string]interface{}{
				"errMsg":    "",
				"tid":       transactionID,
				"timestamp": time.Now(),
			}
			return message, nil
		}

		err = fmt.Errorf(response.Status.Message)
		logger.Error(err)
		message := map[string]interface{}{
			"errMsg":    response.Status.Message,
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}

	// timed out
	err = fmt.Errorf("unable to add a Credential - credential service is not responding")
	logger.WithError(err).Error("timeout")
	message := map[string]interface{}{
		"errMsg":    "unable to add a Credential - credential service is not responding",
		"tid":       transactionID,
		"timestamp": time.Now(),
	}
	return message, err
}
func (credsvc *natsCredentialClient) List() ([]Credential, error) {
	log.Trace("natsCredentialClient.List Called")

	nc, err := nats.Connect(credsvc.natsUrl, nats.MaxReconnects(credsvc.maxReconnect), nats.ReconnectWait(credsvc.reconnectWait))
	if err != nil {
		return nil, err
	}
	defer nc.Close()

	credential := CredentialModel{Session: Session{SessionActor: credsvc.actor, SessionEmulator: credsvc.emulator},
		Username: credsvc.actor}

	ce, err := messaging.CreateCloudEvent(credential, NatsSubjectCredentialsList, credsvc.clientID)

	if err != nil {
		return nil, err
	}

	bytes, err := json.Marshal(ce)

	if err != nil {
		return nil, err
	}

	newctx, cancel := context.WithTimeout(credsvc.ctx, credsvc.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, NatsSubjectCredentialsList, bytes)
	cancel()

	if err != nil {
		return nil, err
	}

	// convert the message to a cloud event
	log.Trace("natsCredsclient.List: received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return nil, err
	}

	var list []CredentialModel
	var credslist []Credential

	err = json.Unmarshal(ce.Data(), &list)
	if err != nil {
		return nil, err
	}

	for i := range list {
		credslist = append(credslist, &list[i])
	}

	return credslist, err
}

func (credsvc *natsCredentialClient) Delete(ID string) (map[string]interface{}, error) {
	log.Trace("natsCredentialClient.Delete() started")

	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsCredentialClient.Delete",
	})

	transactionID := messaging.NewTransactionID()

	if len(ID) == 0 {
		message := map[string]interface{}{
			"errMsg":    "invalid ID format",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, fmt.Errorf("invalid ID format")
	}

	SenderNatsConfig := credsvc.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &credsvc.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		message := map[string]interface{}{
			"errMsg":    "Unable to connect to NATS Streaming",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := credsvc.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, credsvc.stanConfig)

	request := CredentialModel{
		Username: credsvc.actor,
		ID:       ID,
	}

	eventsSubscribe := []common.EventType{
		EventCredentialDeleteError,
		EventCredentialDeleted,
	}

	responseChannel := make(chan common.ServiceRequestResult)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == EventCredentialDeleted || ev == EventCredentialDeleteError {
			var cred CredentialModel
			err := json.Unmarshal(ce.Data(), &cred)
			if err != nil {
				logger.WithError(err).Error("unable to unmarshal a credential delete response")
				responseChannel <- common.ServiceRequestResult{
					Data: nil,
					Status: common.HTTPStatus{
						Message: "unable to unmarshal a credential delete response",
						Code:    http.StatusInternalServerError,
					},
				}
				return
			}
			errType, errMsg := cred.GetError()
			if errType != "" {
				responseChannel <- common.ServiceRequestResult{
					Data: nil,
					Status: common.HTTPStatus{
						Message: fmt.Sprintf("%s, %s", errType, errMsg),
						Code:    http.StatusInternalServerError, // TODO derive error code from ErrorType
					},
				}
				return
			}
			responseChannel <- common.ServiceRequestResult{
				Data: cred,
				Status: common.HTTPStatus{
					Message: "",
					Code:    http.StatusOK,
				},
			}
			return
		}

		responseChannel <- common.ServiceRequestResult{
			Data: nil,
			Status: common.HTTPStatus{
				Message: fmt.Sprintf("unknown event type %s", ev),
				Code:    http.StatusInternalServerError,
			},
		}
	}
	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithError(err).Error("unable to add an event listener")
		message := map[string]interface{}{
			"errMsg":    "unable to add an event listener",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(EventCredentialDeleteRequested, request, transactionID)
	if err != nil {
		logger.WithError(err).Error("unable to publish a delete credential event")
		message := map[string]interface{}{
			"errMsg":    "unable to publish a delete credential event",
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}

	// 	wait for response
	//	Uses workspace response function, but it's generic
	responseReceived, response := waitCredentialResponse(responseChannel, credsvc.requestTimeout*time.Second)
	if responseReceived {
		if response.Status.Code == http.StatusAccepted || response.Status.Code == http.StatusOK {
			message := map[string]interface{}{
				"errMsg":    "",
				"tid":       transactionID,
				"timestamp": time.Now(),
			}
			return message, nil
		}

		err = fmt.Errorf(response.Status.Message)
		logger.Error(err)
		message := map[string]interface{}{
			"errMsg":    response.Status.Message,
			"tid":       transactionID,
			"timestamp": time.Now(),
		}
		return message, err
	}

	// timed out
	err = fmt.Errorf("unable to delete a Credential - credential service is not responding")
	logger.WithError(err).Error("timeout")
	message := map[string]interface{}{
		"errMsg":    "unable to delete a Credential - credential service is not responding",
		"tid":       transactionID,
		"timestamp": time.Now(),
	}
	return message, err

}
