package service

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// DeploymentClient is interface to interact with the Deployment Service.
// Async methods only publish the request, they do NOT wait for response.
// Sync method will generate new transaction ID if empty.
type DeploymentClient interface {
	Get(deploymentID common.ID) (*Deployment, error)
	Search(option DeploymentListOption) (DeploymentList, error)
	SearchNext(list DeploymentList) (DeploymentList, error)
	GetRun(deployment, run common.ID) (*DeploymentRun, error)
	SearchRun(option DeploymentRunListOption) (DeploymentRunList, error)
	SearchRunNext(list DeploymentRunList) (DeploymentRunList, error)
	GetRawState(deployment, run common.ID) ([]byte, error)
	GetLogs(deployment, run common.ID) (string, error)

	// Async methods

	CreateAsync(tid common.TransactionID, option DeploymentCreateParam) error
	RefreshStateAsync(tid common.TransactionID, deployment common.ID) error
	UpdateAsync(tid common.TransactionID, deployment Deployment, updateFields []string) error
	UpdateParametersAsync(tid common.TransactionID, deployment common.ID, param DeploymentParameterValues) error
	DeleteAsync(tid common.TransactionID, deployment common.ID) error
	CreateRunAsync(tid common.TransactionID, param DeploymentRunCreateParam) error
	//StartMonitoringAsync(deployment common.ID) error
	//StopMonitoringAsync(deployment common.ID) error

	// Sync methods

	Create(tid common.TransactionID, option DeploymentCreateParam) (deployment common.ID, err error)
	RefreshState(tid common.TransactionID, deployment common.ID) (DeploymentStateView, error)
	Update(tid common.TransactionID, deployment Deployment, updateFields []string) (common.ID, error)
	UpdateParameters(tid common.TransactionID, deployment common.ID, param DeploymentParameterValues) (runID common.ID, err error)
	// Delete sends a deletion request and listen for a response.
	// Because deletion can be an async process (it needs time to clean up provisioned resources if any), when this call
	// return with no error, the deployment could still be in the deletion process.
	// The boolean return value indicate whether deployment has been deleted by this call.
	Delete(tid common.TransactionID, deployment common.ID) (deleted bool, err error)
	CreateRun(tid common.TransactionID, param DeploymentRunCreateParam) (*DeploymentRun, error)
}

// DeploymentIDPrefix is the prefix for deployment ID
const DeploymentIDPrefix string = "deployment"

// Deployment ...
type Deployment struct {
	ID            common.ID               `json:"id"`
	Name          string                  `json:"name"`
	Description   string                  `json:"description"`
	CreatedAt     time.Time               `json:"created_at"`
	UpdatedAt     time.Time               `json:"updated_at"`
	Owner         string                  `json:"owner"`
	Workspace     common.ID               `json:"workspace"`
	Template      common.ID               `json:"template"`
	TemplateType  string                  `json:"template_type"`
	PrimaryCloud  common.ID               `json:"primary_cloud"`
	CurrentStatus DeploymentStatus        `json:"current_status"`
	PendingStatus DeploymentPendingStatus `json:"pending_status"`
	StatusMsg     string                  `json:"status_msg"`
	// map[CredentialID]ProviderID
	CloudCredentials map[string]common.ID `json:"cloud_credentials"`
	GitCredential    string               `json:"git_credential"`
	LastRun          *DeploymentRun       `json:"last_run"`
}

const (
	// DeploymentStatusNone is the initial status for deployment, when deployment has no run.
	DeploymentStatusNone DeploymentStatus = "none"
	// DeploymentStatusActive is the status for deployment that has 1+ run, and last run succeeds.
	DeploymentStatusActive DeploymentStatus = "active"
	// DeploymentStatusDeleted is the status when a deployment is deleted successfully.
	DeploymentStatusDeleted DeploymentStatus = "deleted"
	// DeploymentStatusCreationErrored is the status when a deployment's last run failed.
	DeploymentStatusCreationErrored DeploymentStatus = "creation_errored"
	// DeploymentStatusDeletionErrored is the status when the deletion failed.
	DeploymentStatusDeletionErrored DeploymentStatus = "deletion_errored"
)

// DeploymentStatus is the status of a deployment, note that it is different from the status of a run.
type DeploymentStatus string

// String ...
func (ds DeploymentStatus) String() string {
	return string(ds)
}

// Valid ...
func (ds DeploymentStatus) Valid() bool {
	switch ds {
	default:
		return false
	case DeploymentStatusNone:
		return true
	case DeploymentStatusActive:
		return true
	case DeploymentStatusDeleted:
		return true
	case DeploymentStatusCreationErrored:
		return true
	case DeploymentStatusDeletionErrored:
		return true
	}
}

// AvailableTransition shows what pending status is available based on the current status.
// return nil if status is not valid status.
func (ds DeploymentStatus) AvailableTransition() []DeploymentPendingStatus {
	switch ds {
	case DeploymentStatusNone:
		return []DeploymentPendingStatus{DeploymentStatusNoPending, DeploymentStatusCreating, DeploymentStatusDeleting}
	case DeploymentStatusActive:
		return []DeploymentPendingStatus{DeploymentStatusNoPending, DeploymentStatusCreating, DeploymentStatusDeleting}
	case DeploymentStatusDeleted:
		return []DeploymentPendingStatus{DeploymentStatusNoPending}
	case DeploymentStatusCreationErrored:
		return []DeploymentPendingStatus{DeploymentStatusNoPending, DeploymentStatusCreating, DeploymentStatusDeleting}
	case DeploymentStatusDeletionErrored:
		return []DeploymentPendingStatus{DeploymentStatusNoPending, DeploymentStatusDeleting}
	default:
		return nil
	}
}

// DeploymentStatusAllowRunCreation is list of current status that allows run creation.
var DeploymentStatusAllowRunCreation = []DeploymentStatus{
	DeploymentStatusNone,
	DeploymentStatusActive,
	DeploymentStatusCreationErrored,
}

// DeploymentStatusAllowDeletion is list of current status that allows deletion.
var DeploymentStatusAllowDeletion = []DeploymentStatus{
	DeploymentStatusNone,
	DeploymentStatusActive,
	DeploymentStatusCreationErrored,
	DeploymentStatusDeletionErrored,
}

var deploymentStatusTransitionMap = map[DeploymentStatus]map[DeploymentPendingStatus]struct{}{
	DeploymentStatusNone: {
		DeploymentStatusNoPending: struct{}{},
		DeploymentStatusCreating:  struct{}{},
		DeploymentStatusDeleting:  struct{}{},
	},
	DeploymentStatusActive: {
		DeploymentStatusNoPending: struct{}{},
		DeploymentStatusCreating:  struct{}{},
		DeploymentStatusDeleting:  struct{}{},
	},
	DeploymentStatusDeleted: {
		DeploymentStatusNoPending: struct{}{},
	},
	DeploymentStatusCreationErrored: {
		DeploymentStatusNoPending: struct{}{},
		DeploymentStatusCreating:  struct{}{},
		DeploymentStatusDeleting:  struct{}{},
	},
	DeploymentStatusDeletionErrored: {
		DeploymentStatusNoPending: struct{}{},
		DeploymentStatusDeleting:  struct{}{},
	},
}

// ValidPendingStatus ...
func (ds DeploymentStatus) ValidPendingStatus(pendingStatus DeploymentPendingStatus) bool {
	subMap, ok := deploymentStatusTransitionMap[ds]
	if !ok {
		// invalid deployment status
		return false
	}
	_, ok = subMap[pendingStatus]
	return ok
}

// AllowRunCreation ...
func (ds DeploymentStatus) AllowRunCreation() bool {
	switch ds {
	case DeploymentStatusNone:
		return true
	case DeploymentStatusActive:
		return true
	case DeploymentStatusCreationErrored:
		return true
	default:
		return false
	}
}

// AllowDeletion ...
func (ds DeploymentStatus) AllowDeletion() bool {
	switch ds {
	case DeploymentStatusNone:
		return true
	case DeploymentStatusActive:
		return true
	case DeploymentStatusCreationErrored:
		return true
	case DeploymentStatusDeletionErrored:
		return true
	default:
		return false
	}
}

const (
	// DeploymentStatusNoPending means that the deployment has no pending transition.
	DeploymentStatusNoPending DeploymentPendingStatus = "none"
	// DeploymentStatusCreating is the pending status when a run is running.
	DeploymentStatusCreating DeploymentPendingStatus = "creating"
	// DeploymentStatusDeleting is the pending status when a deployment is deleting.
	DeploymentStatusDeleting DeploymentPendingStatus = "deleting"
)

// DeploymentPendingStatus is a pending status of deployment, it represents a pending transition that occurs on the deployment object.
type DeploymentPendingStatus string

// String ...
func (ps DeploymentPendingStatus) String() string {
	return string(ps)
}

// Valid ...
func (ps DeploymentPendingStatus) Valid() bool {
	switch ps {
	case DeploymentStatusNoPending:
		return true
	case DeploymentStatusCreating:
		return true
	case DeploymentStatusDeleting:
		return true
	default:
		return false
	}
}

// DeploymentParameter is one instance of parameter to deployment
type DeploymentParameter struct {
	Name  string      `json:"name"`
	Type  string      `json:"type"`
	Value interface{} `json:"value"`
}

// DeploymentParameterValues is the values of parameters
type DeploymentParameterValues map[string]interface{} // map[ParamName]ParamValue

// DeploymentCreateParam are parameter needed to create a new deployment.
type DeploymentCreateParam struct {
	Name         string    `json:"name"`
	Description  string    `json:"description"`
	Workspace    common.ID `json:"workspace"`
	Template     common.ID `json:"template"`
	PrimaryCloud common.ID `json:"primary_cloud"`
	// all cloud providers that this deployment operates on
	CloudProviders []common.ID `json:"cloud_providers"`
	// map[CredentialID]ProviderID
	CloudCredentials map[string]common.ID `json:"cloud_credentials"`
	// if private git repo
	GitCredential string `json:"git_credential"`
}

// DeploymentList is a list of deployment with helper for pagination.
type DeploymentList interface {
	GetOptions() DeploymentListOption
	GetDeployments() []Deployment
	GetStart() int     // index of start of current list, -1 if no results or error
	GetSize() int      // size of the current list, 0 if nothing is found, -1 if error
	GetNextStart() int // index of next partition to retrieve, -1 if error is found
}

type deploymentListModel struct {
	Option      DeploymentListOption
	Deployments []Deployment
}

func (m deploymentListModel) GetOptions() DeploymentListOption {
	return m.Option
}

func (m deploymentListModel) GetDeployments() []Deployment {
	return m.Deployments
}

func (m deploymentListModel) GetStart() int {
	return m.Option.Offset
}

func (m deploymentListModel) GetSize() int {
	return len(m.Deployments)
}

func (m deploymentListModel) GetNextStart() int {
	return m.Option.Offset + len(m.Deployments)
}

// DeploymentListOption is option for fetching a list of deployment.
type DeploymentListOption struct {
	SortBy        SortByField
	SortDirection SortDirection
	Filter        DeploymentFilter
	// offset that the result list should begins on
	Offset   int
	PageSize int
	// Return full run object for the CurrentRun/LastRun, if false then only the Run ID is populated for CurrentRun/LastRun
	FullRunObject bool `json:"full_run"`
}

// SortByField is the field to sort the results by
type SortByField string

// Fields to sort by
const (
	SortByID             SortByField = "id"
	SortByWorkspace      SortByField = "workspace"
	SortByTemplate       SortByField = "template"
	SortByPrimaryCloud   SortByField = "primary_cloud"
	SortByStatus         SortByField = "status"
	SortByStateUpdatedAt SortByField = "state_updated_at"
)

// DeploymentFilter are filters available on deployment list. Only results matched the set filter will be retained.
type DeploymentFilter struct {
	User                 string                  `json:"user,omitempty"`
	Template             common.ID               `json:"template,omitempty"`
	TemplateSet          []common.ID             `json:"template_set,omitempty"`
	Workspace            common.ID               `json:"workspace,omitempty"`
	PrimaryCloudProvider common.ID               `json:"primary_provider,omitempty"`
	LastRunStatus        string                  `json:"last_run_status,omitempty"`
	CurrentStatus        DeploymentStatus        `json:"current_status,omitempty"`
	PendingStatus        DeploymentPendingStatus `json:"pending_status,omitempty"`
	// list deployment with any current status, this override the CurrentStatus filter
	AllCurrentStatus bool `json:"all_current_status,omitempty"`
}

// RunIDPrefix is the prefix for run ID
const RunIDPrefix string = "run"

// DeploymentRun is the result of running the deployment.
// For VM-based, it happens each time the template is run/executed/applied.
// Including initial creation, re-apply after parameter updates, explicit re-apply requested by user.
type DeploymentRun struct {
	ID         common.ID             `json:"id"`
	Deployment common.ID             `json:"deployment"`
	Owner      string                `json:"owner"`
	Start      time.Time             `json:"start"`
	End        time.Time             `json:"end"`
	GitURL     string                `json:"git_url"`
	CommitHash string                `json:"commit"`
	Parameters []DeploymentParameter `json:"parameters"`
	// map[CredentialID]ProviderID
	CloudCredentials map[string]common.ID `json:"cloud_credentials"`
	GitCredential    string               `json:"git_credential"`
	Status           string               `json:"status"`
	StatusMsg        string               `json:"status_msg"`
	// most recent state
	LastState      DeploymentStateView `json:"last_state"`
	StateUpdatedAt time.Time           `json:"state_updated_at"`

	// TODO maybe for later
	//StateHistory []DeploymentStateHistory
}

// DeploymentRunCreateParam are parameters that are needed to create a deployment run
type DeploymentRunCreateParam struct {
	Deployment  common.ID                 `json:"deployment"`
	ParamValues DeploymentParameterValues `json:"param_values"`
}

// DeploymentRunList is an list of deployment runs with pagination helper
type DeploymentRunList interface {
	GetOption() DeploymentRunListOption
	GetRuns() []DeploymentRun
	GetStart() int     // index of start of current list, -1 if no results or error
	GetSize() int      // size of the current list, 0 if nothing is found, -1 if error
	GetNextStart() int // index of next partition to retrieve, -1 if error is found
}

type deploymentRunListModel struct {
	Runs   []DeploymentRun
	Option DeploymentRunListOption
}

func (m deploymentRunListModel) GetOption() DeploymentRunListOption {
	return m.Option
}

func (m deploymentRunListModel) GetRuns() []DeploymentRun {
	return m.Runs
}

func (m deploymentRunListModel) GetStart() int {
	return m.Option.Offset
}

func (m deploymentRunListModel) GetSize() int {
	return len(m.Runs)
}

func (m deploymentRunListModel) GetNextStart() int {
	return m.Option.Offset + len(m.Runs)
}

// DeploymentRunListOption is option available for fetching a list of deployment runs.
type DeploymentRunListOption struct {
	Deployment common.ID
	// offset that the result list should begins on
	Offset   int
	PageSize int
}
