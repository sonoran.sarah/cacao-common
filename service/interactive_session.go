package service

// InteractiveSessionClient ...
//  @Description: The ISMS Client contains three methods: Create, List and Check.
//		Create: is used to create and interactive session: Please provide the following fields when creating a session:
//			Username // (String)
//			InstanceID // (String)
//			Address // IP Address of the instance (String)
//			Protocol // Either SSH or VNC (String)
//			CloudID // Identifier of the cloud where the instance is hosted. (String)
//		Please note that both the InteractiveSession object and the Create function both return an Error object.
//		Both will need to be checked as the Create error returns errors related to the Cacao-Common and the Error
//		string inside the object refers to Errors that happen within the microservice.
//
//		List: Is used to list all active interactive sessions stored in the database. As of 11/30/21 this is stored
//		in a MongoDB instance. For simplicity this function requests a model InteractiveSession. It is designed this way
//		so that future developments can implement the currently unused limit/offset variables. Currently the only needed
//		field is the username field.
//			Username // String
//			Limit // String optional (Unused)
//			Offset // String optional (Unused)
//
//		Check: Is a helper function that determines if the provided InteractiveSession supports VNC connections. This
//		function will ping the microservice which will then run an Ansible script to determine whether or not a desktop
//		environment is installed. Please provide the following fields when using this function.
//			Username // (String)
//			InstanceID // (String)
//			Address // IP Address of the instance (String)
//			Protocol // Either SSH or VNC (String)
//			CloudID // Identifier of the cloud where the instance is hosted. (String)
type InteractiveSessionClient interface {
	Create(is InteractiveSession) (InteractiveSession, error)
	List(is InteractiveSession) ([]InteractiveSession, error)
	Check(is InteractiveSession) (bool, error)
}

// InteractiveSession ...
//  @Description: The proper InteractiveSession object containing basic setters and getters for the ISMS object.
//		Note: As of 11/30/21 the Offset and Limit variables are unused. These are/were intended to filter through large
//		Lists of interactive-Sessions. Perhaps as some sort of pagination system.
type InteractiveSession interface {
	SessionContext
	GetPrimaryID() string
	SetPrimaryID(string)

	GetUsername() string
	SetUsername(string)

	GetOffset() int //UNUSED
	SetOffset(int)  //UNUSED

	GetLimit() int //UNUSED
	SetLimit(int)  //UNUSED

	GetInstanceID() string
	SetInstanceID(string)

	GetAddress() string
	SetAddress(string)

	GetCloudID() string //	Must match an existing GuacLiteServer
	SetCloudID(string)  //	Must match an existing GuacLiteServer

	GetProtocol() string
	SetProtocol(string)

	GetRedirectURL() string
	SetRedirectURL(string)

	Verify() (bool, error)
}

// InteractiveSessionModel ...
//  @Description: The proper model for the ISMS. Containing all needed fields both incoming and outgoing.
//		Username:
//		Offset: Used in the List function. Offset from the first item in the database. //UNUSED
//		Limit: Used in the List function. Limit of items to return from the database, offset by Offset. //UNUSED
//		Address: IP-Address of the instance to connect to.
//		InstanceID: ID of the instance to connect to.
//		CloudID: ID of the cloud to connect to.
//		Protocol: Should be either VNC or SSH
//		RedirectURL: Contains the URL containing both IP+Encrypted Key returned by the ISMS. Used to redirect the user
//			to a Guacamole Lite server.
//		Error: If any Errors happened during setup of the IS in the micro-service this field will contain a string.
type InteractiveSessionModel struct {
	Session
	Username    string `json:"username"`
	Offset      int    `json:"offset"` //UNUSED
	Limit       int    `json:"limit"`  //UNUSED
	Address     string `json:"address"`
	InstanceID  string `json:"instance_id"`
	CloudID     string `json:"cloud_id"`
	Protocol    string `json:"protocol"`
	RedirectURL string `json:"redirect_url"`
	Error       string `json:"error"`
}

func (i InteractiveSessionModel) Verify() (bool, error) {
	//TODO
	return true, nil
}

func (i InteractiveSessionModel) GetPrimaryID() string {
	return i.InstanceID
}

func (i InteractiveSessionModel) SetPrimaryID(s string) {
	i.InstanceID = s
}

func (i InteractiveSessionModel) GetUsername() string {
	return i.Username
}

func (i InteractiveSessionModel) SetUsername(s string) {
	i.Username = s
}

func (i InteractiveSessionModel) GetCloudID() string {
	return i.CloudID
}

func (i InteractiveSessionModel) SetCloudID(s string) {
	i.CloudID = s
}

func (i InteractiveSessionModel) GetAddress() string {
	return i.Address
}

func (i InteractiveSessionModel) SetAddress(s string) {
	i.Address = s
}

func (i InteractiveSessionModel) GetOffset() int { //UNUSED
	return i.Offset
}

func (i InteractiveSessionModel) SetOffset(i2 int) { //UNUSED
	i.Offset = i2
}

func (i InteractiveSessionModel) GetLimit() int { //UNUSED
	return i.Limit
}

func (i InteractiveSessionModel) SetLimit(i2 int) { //UNUSED
	i.Limit = i2
}

func (i InteractiveSessionModel) GetInstanceID() string {
	return i.GetPrimaryID()
}

func (i InteractiveSessionModel) SetInstanceID(s string) {
	i.SetPrimaryID(s)
}

func (i InteractiveSessionModel) GetProtocol() string {
	return i.Protocol
}

func (i InteractiveSessionModel) SetProtocol(s string) {
	i.Protocol = s
}

func (i InteractiveSessionModel) GetRedirectURL() string {
	return i.RedirectURL
}

func (i InteractiveSessionModel) SetRedirectURL(s string) {
	i.RedirectURL = s
}
