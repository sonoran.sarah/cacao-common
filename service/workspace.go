package service

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// WorkspaceClient is the client for Workspace
type WorkspaceClient interface {
	Get(workspaceID common.ID) (Workspace, error)
	List() ([]Workspace, error)

	Create(workspace Workspace) (common.ID, error)
	Update(workspace Workspace) error
	UpdateFields(workspace Workspace, updateFieldNames []string) error
	Delete(workspaceID common.ID) error
}

// Workspace is the standard interface for all Workspace implementations
type Workspace interface {
	SessionContext

	GetPrimaryID() common.ID // points to ID
	GetID() common.ID
	GetOwner() string
	GetName() string
	GetDescription() string
	GetDefaultProviderID() common.ID
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetUpdateFieldNames() []string

	SetPrimaryID(id common.ID)
	SetID(id common.ID)
	SetName(name string)
	SetDescription(description string)
	SetDefaultProviderID(id common.ID)
	SetUpdateFieldNames(fieldNames []string)
}

// WorkspaceModel is the workspace model
type WorkspaceModel struct {
	Session

	ID                common.ID `json:"id"`
	Owner             string    `json:"owner"`
	Name              string    `json:"name"`
	Description       string    `json:"description,omitempty"`
	DefaultProviderID common.ID `json:"default_provider_id,omitempty"`
	CreatedAt         time.Time `json:"created_at"`
	UpdatedAt         time.Time `json:"updated_at"`

	UpdateFieldNames []string `json:"update_field_names,omitempty"`
}

// NewWorkspaceID generates a new WorkspaceID
func NewWorkspaceID() common.ID {
	return common.NewID("workspace")
}

// GetPrimaryID returns the primary ID of the Workspace
func (w *WorkspaceModel) GetPrimaryID() common.ID {
	return w.ID
}

// GetID returns the ID of the Workspace
func (w *WorkspaceModel) GetID() common.ID {
	return w.ID
}

// GetOwner returns the owner of the Workspace
func (w *WorkspaceModel) GetOwner() string {
	return w.Owner
}

// GetName returns the name of the Workspace
func (w *WorkspaceModel) GetName() string {
	return w.Name
}

// GetDescription returns the description of the Workspace
func (w *WorkspaceModel) GetDescription() string {
	return w.Description
}

// GetDefaultProviderID returns the default provider ID of the Workspace
func (w *WorkspaceModel) GetDefaultProviderID() common.ID {
	return w.DefaultProviderID
}

// GetCreatedAt returns the creation time of the workspace
func (w *WorkspaceModel) GetCreatedAt() time.Time {
	return w.CreatedAt
}

// GetUpdatedAt returns the modified time of the workspace
func (w *WorkspaceModel) GetUpdatedAt() time.Time {
	return w.UpdatedAt
}

// GetUpdateFieldNames returns the field names to be updated
func (w *WorkspaceModel) GetUpdateFieldNames() []string {
	return w.UpdateFieldNames
}

// SetPrimaryID sets the primary ID of the Workspace
func (w *WorkspaceModel) SetPrimaryID(id common.ID) {
	w.ID = id
}

// SetID sets the ID of the Workspace
func (w *WorkspaceModel) SetID(id common.ID) {
	w.ID = id
}

// SetName sets the name of the Workspace
func (w *WorkspaceModel) SetName(name string) {
	w.Name = name
}

// SetDescription sets the description of the Workspace
func (w *WorkspaceModel) SetDescription(description string) {
	w.Description = description
}

// SetDefaultProviderID sets the default provider ID of the Workspace
func (w *WorkspaceModel) SetDefaultProviderID(id common.ID) {
	w.DefaultProviderID = id
}

// SetUpdateFieldNames sets the field names to be updated
func (w *WorkspaceModel) SetUpdateFieldNames(fieldNames []string) {
	w.UpdateFieldNames = fieldNames
}

// WorkspaceListItemModel is the element type in WorkspaceListModel
type WorkspaceListItemModel struct {
	ID                common.ID `json:"id"`
	Owner             string    `json:"owner"`
	Name              string    `json:"name"`
	Description       string    `json:"description,omitempty"`
	DefaultProviderID common.ID `json:"default_provider_id,omitempty"`
	CreatedAt         time.Time `json:"created_at"`
	UpdatedAt         time.Time `json:"updated_at"`
}

// WorkspaceListModel is the workspace list model
type WorkspaceListModel struct {
	Session

	Workspaces []WorkspaceListItemModel `json:"workspaces"`
}

// GetWorkspaces returns workspaces
func (w *WorkspaceListModel) GetWorkspaces() []Workspace {
	workspaces := make([]Workspace, 0, len(w.Workspaces))

	for _, workspace := range w.Workspaces {
		workspaceModel := WorkspaceModel{
			Session:           w.Session,
			ID:                workspace.ID,
			Owner:             workspace.Owner,
			Name:              workspace.Name,
			Description:       workspace.Description,
			DefaultProviderID: workspace.DefaultProviderID,
			CreatedAt:         workspace.CreatedAt,
			UpdatedAt:         workspace.UpdatedAt,
		}

		workspaces = append(workspaces, &workspaceModel)
	}

	return workspaces
}
