package service

import (
	"errors"
	"time"
)

const (
//DefaultCredentialListItemsMax = 10 // Default maximum number of Credential items to return
)

type SecretType string
type VisibilityType string

type CredentialClient interface {
	Add(secret Credential) (map[string]interface{}, error)
	Update(secret Credential) (map[string]interface{}, error)
	Get(ID string) (Credential, error)
	Delete(ID string) (map[string]interface{}, error)
	List() ([]Credential, error)
}

type Credential interface {
	SessionContext
	GetPrimaryID() string // points to username+"/"+id
	GetUsername() string
	GetValue() string
	GetType() SecretType
	GetID() string

	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetUpdatedBy() string
	GetUpdatedEmulatorBy() string
	GetDescription() string
	GetSystemStatus() bool
	GetHiddenStatus() bool
	GetVisibility() VisibilityType
	GetTags() []string

	SetUsername(string)
	SetValue(string)
	SetType(SecretType)
	SetID(string)
	SetDescription(string)
	SetSystemStatus(bool)
	SetHiddenStatus(bool)
	SetVisibilityStatus(visibilityType VisibilityType)

	AddTag(string)
	RemoveTag(string)
	RemoveAllTags()

	Verify() (bool, error)
}

type CredentialModel struct {
	Session
	Username          string         `json:"username,omitempty"`
	Value             string         `json:"value,omitempty"`
	Type              SecretType     `json:"type,omitempty"`
	ID                string         `json:"id,omitempty"`
	Description       string         `json:"description,omitempty"`
	IsSystem          bool           `json:"is_system,omitempty"`
	IsHidden          bool           `json:"is_hidden,omitempty"`
	Visibility        VisibilityType `json:"visibility,omitempty"`
	Tags              []string       `json:"tags,omitempty"`
	CreatedAt         time.Time      `json:"created_at"`
	UpdatedAt         time.Time      `json:"updated_at"`
	UpdatedBy         string         `json:"updated_by"`
	UpdatedEmulatorBy string         `json:"updated_emulator_by"`
}

// GetRedacted will return a new Secret with the Value set to "REDACTED"
func (c *CredentialModel) GetRedacted() CredentialModel {
	redactedSecret := *c
	redactedSecret.Value = "REDACTED"
	return redactedSecret
}

func (c *CredentialModel) Verify() (bool, error) {
	if len(c.Username) <= 0 {
		return false, errors.New("username not set, could not verify")
	}
	if len(c.Value) <= 0 {
		return false, errors.New("value not set, could not verify")
	}
	if len(c.ID) <= 0 {
		return false, errors.New("ID not set, could not verify")
	}
	if len(c.Type) <= 0 {
		return false, errors.New("Type not set, could not verify")
	}

	return true, nil
}

func (c *CredentialModel) SetUsername(s string) {
	if c.Username != s {
		c.Username = s
	}
}

func (c *CredentialModel) SetValue(s string) {
	if c.Value != s {
		c.Value = s
	}
}

func (c *CredentialModel) SetType(s SecretType) {
	if c.Type != s {
		c.Type = s
	}
}

func (c *CredentialModel) SetID(s string) {
	if c.ID != s {
		c.ID = s
	}
}

func (c *CredentialModel) SetDescription(s string) {
	c.Description = s
}

func (c *CredentialModel) SetHiddenStatus(s bool) {
	c.IsHidden = s
}

func (c *CredentialModel) SetSystemStatus(s bool) {
	c.IsSystem = s
}

func (c *CredentialModel) SetVisibilityStatus(s VisibilityType) {
	c.Visibility = s
}

func (c *CredentialModel) AddTag(s string) {
	c.Tags = append(c.Tags, s)
}

func (c *CredentialModel) RemoveTag(s string) {
	var sl = -1
	for r, x := range c.Tags {
		if x == s {
			sl = r
			break
		}
	}
	if sl == -1 {
		return
	}
	c.Tags = append(c.Tags[:sl], c.Tags[sl+1:]...)
}

func (c CredentialModel) GetPrimaryID() string {
	return c.Username + "/" + c.ID
}
func (c CredentialModel) GetUsername() string {
	return c.Username
}
func (c CredentialModel) GetCreatedAt() time.Time {
	return c.CreatedAt
}
func (c CredentialModel) GetUpdatedAt() time.Time {
	return c.CreatedAt
}
func (c CredentialModel) GetUpdatedBy() string {
	return c.UpdatedBy
}
func (c CredentialModel) GetUpdatedEmulatorBy() string {
	return c.UpdatedEmulatorBy
}

func (c CredentialModel) GetValue() string {
	return c.Value
}

func (c CredentialModel) GetType() SecretType {
	return c.Type
}

func (c CredentialModel) GetID() string {
	return c.ID
}

func (c CredentialModel) GetDescription() string {
	return c.Description
}

func (c CredentialModel) GetSystemStatus() bool {
	return c.IsSystem
}

func (c CredentialModel) GetHiddenStatus() bool {
	return c.IsHidden
}

func (c CredentialModel) GetVisibility() VisibilityType {
	return c.Visibility
}

func (c CredentialModel) GetTags() []string {
	return c.Tags
}

func (c CredentialModel) RemoveAllTags() {
	c.Tags = make([]string, 0)
}
