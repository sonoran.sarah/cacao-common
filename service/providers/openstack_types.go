package providers

import (
	"crypto/sha256"
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"

	"gitlab.com/cyverse/cacao-common/service"
)

// Client is the interface to the provider service.
type Client interface {
	Image(string) error
	ListImages(*ImageListingArgs) ([]Image, error)
	Flavor(string) error
	ListFlavors(*FlavorListingArgs) ([]Flavor, error)
}

// OpenStackQueryPrefix is prefix for query operation for openstack provider
const OpenStackQueryPrefix common.QueryOp = "cacao.providers.openstack."

// ApplicationCredentialsListOp is the operation for listing credentials.
const ApplicationCredentialsListOp common.QueryOp = "appcredentials.list"

// ApplicationCredentialsGetOp is the operation for getting a single credential.
const ApplicationCredentialsGetOp common.QueryOp = "appcredentials.get"

// ImagesListOp is the operation for listing images.
const ImagesListOp common.QueryOp = "images.list"

// ImagesGetOp is the operation for getting a single image.
const ImagesGetOp common.QueryOp = "images.get"

// FlavorsListOp is the operation for listing flavors.
const FlavorsListOp common.QueryOp = "flavors.list"

// FlavorsGetOp is the operation for getting a single flavor.
const FlavorsGetOp common.QueryOp = "flavors.get"

// ProjectsListOp is the operation for listing projects.
const ProjectsListOp common.QueryOp = "projects.list"

// ProjectsGetOp is the operation for getting a single project.
const ProjectsGetOp common.QueryOp = "projects.get"

// CachePopulateOp is the operation for forcing a cache population for a user.
// const CachePopulateOp common.QueryOp = "cache.populate"

// CredGetterType is the type to represent a type of credentials retriever.
type CredGetterType string

// CredGetterString is the type for getting credentials from a string.
const CredGetterString CredGetterType = "string"

// CredGetterClient is the type for getting credentials from a client.
const CredGetterClient CredGetterType = "client"

// CredGetterError is the type for returning errors for every credentials retrieval attempt.
const CredGetterError CredGetterType = "error"

// Credentials represents auth information about the user.
type Credentials struct {
	ID           string      `json:"id" mapstructure:"id"`
	OpenStackEnv Environment `json:"openstack_env,omitempty" mapstructure:"openstack_env"`
}

// ProviderRequest ...
type ProviderRequest struct {
	service.Session `json:",inline"`
	Operation       common.QueryOp `json:"op"`
	Provider        common.ID      `json:"provider"`
	Credential      string         `json:"credential"`
	// Args depends on the type of query operation
	Args interface{} `json:"args"`
}

// BaseProviderReply ...
type BaseProviderReply struct {
	Session   service.Session `json:",inline"`
	Operation common.QueryOp  `json:"op"`
	Provider  common.ID       `json:"provider"`
}

// GetApplicationCredentialReply ...
type GetApplicationCredentialReply struct {
	BaseProviderReply     `json:",inline"`
	ApplicationCredential *ApplicationCredential `json:"application_credential"`
}

// ToCloudEvent ...
func (r GetApplicationCredentialReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ApplicationCredentialsGetOp), source)
}

// ApplicationCredentialListReply ...
type ApplicationCredentialListReply struct {
	BaseProviderReply      `json:",inline"`
	ApplicationCredentials []ApplicationCredential `json:"application_credentials"`
}

// ToCloudEvent ...
func (r ApplicationCredentialListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ApplicationCredentialsListOp), source)
}

// GetImageReply ...
type GetImageReply struct {
	BaseProviderReply `json:",inline"`
	Image             *Image `json:"image"`
}

// ToCloudEvent ...
func (r GetImageReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ImagesGetOp), source)
}

// ImageListReply ...
type ImageListReply struct {
	BaseProviderReply `json:",inline"`
	Images            []Image `json:"images"`
}

// ToCloudEvent ...
func (r ImageListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ImagesListOp), source)
}

// GetFlavorReply ...
type GetFlavorReply struct {
	BaseProviderReply `json:",inline"`
	Flavor            *Flavor `json:"flavor"`
}

// ToCloudEvent ...
func (r GetFlavorReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+FlavorsGetOp), source)
}

// FlavorListReply ...
type FlavorListReply struct {
	BaseProviderReply `json:",inline"`
	Flavors           []Flavor `json:"flavors"`
}

// ToCloudEvent ...
func (r FlavorListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+FlavorsListOp), source)
}

// GetProjectReply ...
type GetProjectReply struct {
	BaseProviderReply `json:",inline"`
	Project           *Project `json:"project"`
}

// ToCloudEvent ...
func (r GetProjectReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ProjectsGetOp), source)
}

// ProjectListReply ...
type ProjectListReply struct {
	BaseProviderReply `json:",inline"`
	Projects          []Project `json:"projects"`
}

// ToCloudEvent ...
func (r ProjectListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ProjectsListOp), source)
}

// CachePopulateReply ...
// type CachePopulateReply BaseProviderReply

// ToCloudEvent ...
// func (r CachePopulateReply) ToCloudEvent(source string) (cloudevents.Event, error) {
// 	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+CachePopulateOp), source)
// }

// Hashable defines the interface for objects that want to make a checksum available.
type Hashable interface {
	Checksum() []byte
}

// Flavor represents the compute, memory and storage capacity of a computing
// instance in OpenStack (https://docs.openstack.org/nova/latest/user/flavors.html)
type Flavor struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	RAM       int64  `json:"ram"`
	Ephemeral int64  `json:"ephemeral"`
	VCPUs     int64  `json:"vcpus"`
	IsPublic  bool   `json:"is_public"`
	Disk      int64  `json:"disk"`
}

// FlavorListingArgs represents the available options that can be passed to
// `openstack flavor list`.
type FlavorListingArgs struct {
	Public  bool
	Private bool
	All     bool
	Limit   int64
}

// Checksum returns the hashed value of the string representation of
// the FlavorListingArgs.
func (f *FlavorListingArgs) Checksum() []byte {
	hasher := sha256.New()
	return hasher.Sum([]byte(f.String()))
}

// String implements the Stringer interface for FlavorListingArgs. Note: this
// is not the same as the command-line representation.
func (f *FlavorListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v", f.Public, f.Private, f.All, f.Limit)
}

// Environment contains a map of string keys and string values representing the environment
// in which OpenStack CLI commands should execute.  This is important for setting the
// appropriate environment variables for accessing OpenStack
type Environment map[string]string

// Image describes an image available through OpenStack. Adapted from the
// long listing format.
type Image struct {
	ID              string   `json:"id"`
	Name            string   `json:"name"`
	DiskFormat      string   `json:"disk_format"`
	ContainerFormat string   `json:"container_format"`
	Size            int64    `json:"size"`
	Checksum        string   `json:"checksum"`
	Status          string   `json:"status"`
	Visibility      string   `json:"visibility"`
	Protected       bool     `json:"protected"`
	Project         string   `json:"project"`
	Tags            []string `json:"tags"`
}

// ImageListingSortOption contains the key and direction for a sorting
// option passed to the openstack command-line.
type ImageListingSortOption struct {
	Key       string
	Direction string
}

// ImageListingArgs contains the possibly settings for listing images.
type ImageListingArgs struct {
	Public        bool
	Private       bool
	Community     bool
	Shared        bool
	Name          string
	Status        string
	MemberStatus  string
	Project       string
	ProjectDomain string
	Tag           string
	Limit         int64
	SortOpts      []ImageListingSortOption
	Properties    []string // should be in the key=value format
}

// Checksum implements the Hashable interface for ImageListingArgs.
func (i *ImageListingArgs) Checksum() []byte {
	hasher := sha256.New()
	return hasher.Sum([]byte(i.String()))
}

// String implements the String interface for ImageListingArgss. Note:
// this does not return the command-line arguments for the corresponding
// openstack command.
func (i *ImageListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v %v %v %v %v %v %v %v %v %v",
		i.Public,
		i.Private,
		i.Community,
		i.Shared,
		i.Name,
		i.Status,
		i.MemberStatus,
		i.Project,
		i.ProjectDomain,
		i.Tag,
		i.Limit,
		i.SortOpts,
		i.Properties,
	)
}

// Application Credential
type ApplicationCredential struct {
	ID           string        `json:"id"`
	Name         string        `json:"name"`
	Description  string        `json:"description"`
	ExpiresAt    interface{}   `json:"expires_at"`
	ProjectID    string        `json:"project_id"`
	Roles        []interface{} `json:"roles"`
	Unrestricted bool          `json:"unrestricted"`
}

// Project
type Project struct {
	ID          string                 `json:"id"`
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	DomainID    string                 `json:"domain_id"`
	Enabled     bool                   `json:"enabled"`
	IsDomain    bool                   `json:"is_domain"`
	ParentID    string                 `json:"parent_id"`
	Properties  map[string]interface{} `json:"properties"`
}

// StringArgs exists so that the command-line calls that only need
// a single custom argument of type string can still be used for
// generating a cache key.
type StringArgs string

// Checksum implements the Hashable interface for StringArgs and returns
// the SHA256 of the StringArgs.
func (s StringArgs) Checksum() []byte {
	hasher := sha256.New()
	return hasher.Sum([]byte(s))
}

// OpenStackProviderError is what gets returned when an error occurs during the
// processing of a request.
type OpenStackProviderError struct {
	Error         string `json:"error"`
	TransactionID string `json:"transaction_id"`
}
