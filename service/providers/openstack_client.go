package providers

import (
	"context"
	"encoding/json"

	"gitlab.com/cyverse/cacao-common/common"

	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// OpenStackProvider is the interface for all provider services.
// All operations will require a credential, if credential ID is empty (not provided),
// then provider-openstack-service will attempt to select a credential by searching through user's credentials.
// The credential ID needs to be under the name of the session actor.
type OpenStackProvider interface {
	Init() error
	ApplicationCredentialList(ctx context.Context, session *service.Session, credentialID string) ([]ApplicationCredential, error)
	GetApplicationCredential(ctx context.Context, session *service.Session, applicationCredentialID, credentialID string) (*ApplicationCredential, error)
	ImageList(ctx context.Context, session *service.Session, credentialID string) ([]Image, error)
	GetImage(ctx context.Context, session *service.Session, imageID, credentialID string) (*Image, error)
	FlavorList(ctx context.Context, session *service.Session, credentialID string) ([]Flavor, error)
	GetFlavor(ctx context.Context, session *service.Session, flavorID, credentialID string) (*Flavor, error)
	ProjectList(ctx context.Context, session *service.Session, credentialID string) ([]Project, error)
	GetProject(ctx context.Context, session *service.Session, projectID, credentialID string) (*Project, error)
	// PopulateCache(ctx context.Context, session *service.Session, credentialID string) error
	Finish() error
}

// natsOpenStack is an implementation of Provider that sends requests
// over NATS and waits for responses.
type natsOpenStack struct {
	provider common.ID
	// conn           *nats.Conn
	// credGetterType CredGetterType
	nats *messaging.NatsConfig
}

// NewOpenStackProvider returns a new instance of Provider. You will still need to call
// Init() on it.
func NewOpenStackProvider(
	provider common.ID,
	nats *messaging.NatsConfig,
) OpenStackProvider {
	return &natsOpenStack{
		provider: provider,
		nats:     nats,
	}
}

// Init ...
func (p *natsOpenStack) Init() error {
	return nil
}

// Finish ...
func (p *natsOpenStack) Finish() error {
	return nil
}

// CredentialList gets the listing of available credentials and returns it.
func (p *natsOpenStack) ApplicationCredentialList(ctx context.Context, session *service.Session, credentialID string) ([]ApplicationCredential, error) {
	var reply ApplicationCredentialListReply
	if err := p.doOperation(
		ctx,
		*session,
		ApplicationCredentialsListOp,
		credentialID,
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.ApplicationCredentials, nil
}

// GetCredential returns a specific application credential by its UUID.
func (p *natsOpenStack) GetApplicationCredential(ctx context.Context, session *service.Session, id string, credentialID string) (*ApplicationCredential, error) {
	var reply GetApplicationCredentialReply
	if err := p.doOperation(
		ctx,
		*session,
		ApplicationCredentialsGetOp,
		credentialID,
		id,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.ApplicationCredential, nil
}

// ImageList gets the listing of available images and returns it.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) ImageList(ctx context.Context, session *service.Session, credentialID string) ([]Image, error) {
	var reply ImageListReply
	if err := p.doOperation(
		ctx,
		*session,
		ImagesListOp,
		credentialID,
		ImageListingArgs{}, // TODO make use of args after svc supports it
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Images, nil
}

// GetImage returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) GetImage(ctx context.Context, session *service.Session, imageID, credentialID string) (*Image, error) {
	var reply GetImageReply
	if err := p.doOperation(
		ctx,
		*session,
		ImagesGetOp,
		credentialID,
		imageID, // use image ID as args
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Image, nil
}

// FlavorList returns a list of the available flavors.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) FlavorList(ctx context.Context, session *service.Session, credentialID string) ([]Flavor, error) {
	var reply FlavorListReply

	if err := p.doOperation(
		ctx,
		*session,
		FlavorsListOp,
		credentialID,
		FlavorListingArgs{}, // TODO make use of args after svc supports it
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavors, nil
}

// GetFlavor returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) GetFlavor(ctx context.Context, session *service.Session, flavorID, credentialID string) (*Flavor, error) {
	var reply GetFlavorReply
	if err := p.doOperation(
		ctx,
		*session,
		FlavorsGetOp,
		credentialID,
		flavorID, // use flavorID as args
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavor, nil
}

// ProjectList returns a list of the available projects.
func (p *natsOpenStack) ProjectList(ctx context.Context, session *service.Session, credentialID string) ([]Project, error) {
	var reply ProjectListReply

	if err := p.doOperation(
		ctx,
		*session,
		ProjectsListOp,
		credentialID,
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Projects, nil
}

// GetProject returns a specific project by its UUID.
func (p *natsOpenStack) GetProject(ctx context.Context, session *service.Session, id string, credentialID string) (*Project, error) {
	var reply GetProjectReply
	if err := p.doOperation(
		ctx,
		*session,
		ProjectsGetOp,
		credentialID,
		id,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Project, nil
}

// PopulateCache tells the OpenStack provider to fetch the images, flavors, projects, and application credentials for a user
// func (p *natsOpenStack) PopulateCache(ctx context.Context, session *service.Session, credentialID string) error {
// 	var reply CachePopulateReply
// 	if err := p.doOperation(
// 		ctx,
// 		*session,
// 		CachePopulateOp,
// 		credentialID,
// 		nil,
// 		&reply,
// 	); err != nil {
// 		return err
// 	}
// 	if reply.Session.GetServiceError() != nil {
// 		return reply.Session.GetServiceError()
// 	}
// 	return nil
// }

func (p *natsOpenStack) doOperation(
	ctx context.Context,
	session service.Session,
	op common.QueryOp,
	credentialID string,
	args interface{},
	output interface{},
) error {
	conn, err := messaging.ConnectNatsForServiceClient(p.nats)
	if err != nil {
		return service.NewCacaoCommunicationError(err.Error())
	}
	defer conn.Disconnect()
	var subject = p.subject(op)
	reply, err := conn.RequestWithTransactionID(ctx, subject, ProviderRequest{
		Session:    session,
		Operation:  op,
		Provider:   p.provider,
		Credential: credentialID,
		Args:       args,
	}, messaging.NewTransactionID())
	if err != nil {
		return service.NewCacaoCommunicationError(err.Error())
	}
	err = json.Unmarshal(reply, output)
	if err != nil {
		return service.NewCacaoMarshalError(err.Error())
	}
	return nil
}

func (p *natsOpenStack) subject(op common.QueryOp) common.QueryOp {
	return OpenStackQueryPrefix + op
}
