package providers

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// CredGetter defines the interface for objects that can return values from the
// credentials service as a string.
type CredGetter interface {
	Get(id string) (string, string, error)
}

// CredClientCreator defines the interface for objects that can return a new
// client for getting credentials.
// type credClientCreator func(context.Context, *service.Session, *messaging.NatsConfig, *messaging.StanConfig) (CredGetter, error)

// credentialsNATS satisfies the CredGetter interface and will allow callers
// retrieve credentials over NATS.
type credentialsNATS struct {
	client service.CredentialClient
}

// newCredentialsNATS returns a newly created *CredentialsNATS.
func newCredentialsNATS(ctx context.Context, session *service.Session, nats *messaging.NatsConfig, stan *messaging.StanConfig) (CredGetter, error) {
	client, err := service.NewNatsCredentialClient(
		ctx,
		session.SessionActor,
		session.SessionEmulator,
		*nats,
		*stan,
	)
	if err != nil {
		return nil, err
	}
	return &credentialsNATS{
		client: client,
	}, nil
}

// Get returns a Credential from the credentials service as a string.
func (c *credentialsNATS) Get(id string) (string, string, error) {
	cred, err := c.client.Get(id)
	if err != nil {
		return "", "", err
	}
	value := cred.GetValue()
	credID := cred.GetPrimaryID()
	return credID, value, nil
}

// credentialsString allows you to set up a credential as an embedded
// string in the properties file format. Do not use this in production,
// this is mainly intended to make manual testing simpler.
type credentialsString struct {
	filePath string
	id       string
}

// newCredentialsString implements the CredClientCreator interface
// for the ProviderCredentialsString type.
func newCredentialsString(ctx context.Context, session *service.Session, _ *messaging.NatsConfig, _ *messaging.StanConfig) (CredGetter, error) {
	filePath := os.Getenv("OS_CRED_FILE_PATH")
	if filePath == "" {
		filePath = "./.os_creds.json"
	}
	filePath, err := filepath.Abs(filepath.Clean(filePath))
	if err != nil {
		return nil, err
	}
	return &credentialsString{
		filePath: filePath,
		id:       "testing-only",
	}, nil
}

// Get returns the contents of a JSON file as a string. It does this on every
// retrieval so that the performance is slightly more similar to getting the
// credentials over the wire from the service. Do not use this in production.
func (c *credentialsString) Get(id string) (string, string, error) {
	ofile, err := os.Open(c.filePath)
	if err != nil {
		return "", "", err
	}
	output, err := io.ReadAll(ofile)
	return c.id, string(output), err
}

type credentialsErrorEmitter struct{}

func newCredentialsErrorEmitter(ctx context.Context, session *service.Session, _ *messaging.NatsConfig, _ *messaging.StanConfig) (CredGetter, error) {
	var getter credentialsErrorEmitter
	return &getter, nil
}

func (e *credentialsErrorEmitter) Get(id string) (string, string, error) {
	return "", "", errors.New("error from credentialsErrorEmitter")
}

// NewCredentialsClient is the entrypoint into the credentials related code for the providers code.
func NewCredentialsClient(
	ctx context.Context,
	kind CredGetterType,
	session *service.Session,
	nats *messaging.NatsConfig,
	stan *messaging.StanConfig,
) (CredGetter, error) {
	switch kind {
	case CredGetterString:
		return newCredentialsString(ctx, session, nats, stan)
	case CredGetterClient:
		return newCredentialsNATS(ctx, session, nats, stan)
	case CredGetterError:
		return newCredentialsErrorEmitter(ctx, session, nats, stan)
	default:
		return nil, fmt.Errorf("unknown type %s", kind)

	}
}
