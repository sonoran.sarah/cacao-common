package service

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/rs/xid"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"

	nats "github.com/nats-io/nats.go"
)

// Nats Subjects root, get, list and other channels
const (
	NatsSubjectUsers     string = common.NatsQueryOpPrefix + "users"
	NatsSubjectUsersGet  string = NatsSubjectUsers + ".get"
	NatsSubjectUsersList string = NatsSubjectUsers + ".list"

	EventUserLogin common.EventType = "org.cyverse.events.UserLogin"

	EventUserAddRequested    common.EventType = "org.cyverse.events.UserAddRequested"
	EventUserUpdateRequested common.EventType = "org.cyverse.events.UserUpdateRequested"
	EventUserDeleteRequested common.EventType = "org.cyverse.events.UserDeleteRequested"

	EventUserAdded   common.EventType = "org.cyverse.events.UserAdded"
	EventUserUpdated common.EventType = "org.cyverse.events.UserUpdated"
	EventUserDeleted common.EventType = "org.cyverse.events.UserDeleted"

	EventUserAddError    common.EventType = "org.cyverse.events.UserAddError"
	EventUserUpdateError common.EventType = "org.cyverse.events.UserUpdateError"
	EventUserDeleteError common.EventType = "org.cyverse.events.UserDeleteError"
)

// // These are User options
// type NatsUserOptions func(u *natsuserclient)

// func ForcePreferencesOption(user *natsuserclient) { // this option will force the loading and saving of preferences with a User object or User objects in the list
// 	user.User.ForceLoadPreferences = true
// }

// // These are UserList options
// type NatsUserListOptions func(u *NatsUserList)

// func ForcePreferencesListOption(ulist *NatsUserList) { // this option will force the loading and saving of preferences with a User object or User objects in the list
// 	ulist.UserList.ForceLoadPreferences = true
// }

// This object should never be serialized/marshalled
type natsUserClient struct {
	actor          string
	emulator       string
	natsConfig     messaging.NatsConfig
	stanConfig     messaging.StanConfig
	maxReconnect   int
	reconnectWait  time.Duration
	requestTimeout time.Duration
	eventsChannel  string
	eventsTimeout  time.Duration
	ctx            context.Context
	eventSrc       messaging.EventObservable
}

func NewNatsUserClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (UserClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsUserClient",
	})
	logger.Trace("NewNatsUserClient() called")

	if actor == "" {
		return nil, errors.New(UserActorNotSetError)
	}

	if ctx == nil {
		ctx = context.Background()
	}
	new_user_svc := natsUserClient{
		actor:         actor,
		emulator:      emulator,
		natsConfig:    natsConfig,
		stanConfig:    stanConfig,
		eventsChannel: common.EventsSubject,
		ctx:           ctx}

	logger.WithFields(log.Fields{
		"natsUrl":       new_user_svc.natsConfig.URL,
		"clientID":      new_user_svc.natsConfig.ClientID,
		"clusterID":     new_user_svc.stanConfig.ClusterID,
		"eventsChannel": new_user_svc.eventsChannel,
	}).Trace()

	// for _, option := range options { // call the options
	// 	option(&new_user_svc)
	// }

	if natsConfig.MaxReconnects <= 0 {
		new_user_svc.maxReconnect = messaging.DefaultNatsMaxReconnect
	} else {
		new_user_svc.maxReconnect = natsConfig.MaxReconnects
	}
	if natsConfig.ReconnectWait <= 0 {
		new_user_svc.reconnectWait = messaging.DefaultNatsReconnectWait
	} else {
		new_user_svc.reconnectWait = time.Duration(natsConfig.ReconnectWait) * time.Second
	}
	if natsConfig.RequestTimeout <= 0 {
		new_user_svc.requestTimeout = messaging.DefaultNatsRequestTimeout
	} else {
		new_user_svc.requestTimeout = time.Duration(natsConfig.RequestTimeout) * time.Second
	}
	if stanConfig.EventsTimeout <= 0 {
		new_user_svc.eventsTimeout = messaging.DefaultStanEventsTimeout
	} else {
		new_user_svc.eventsTimeout = time.Duration(stanConfig.EventsTimeout) * time.Second
	}

	// create EventSource with a different ClientID
	eventSrcNatsConf := natsConfig
	eventSrcNatsConf.ClientID += "-" + xid.New().String()
	new_user_svc.eventSrc = messaging.NewEventSource(eventSrcNatsConf, stanConfig)

	return &new_user_svc, nil
}

func (usersvc *natsUserClient) Add(user User) error {
	log.Trace("natsuserclient.Add() started")
	return usersvc.publishRequestEvent(user, EventUserAddRequested, true)
}

func (usersvc *natsUserClient) Update(user User) error {
	log.Trace("natsuserclient.Update() started")
	return usersvc.publishRequestEvent(user, EventUserUpdateRequested, true)
}

func waitUserResponse(c chan UserModel, timeout time.Duration) (bool, UserModel) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, UserModel{}
	}
}

// save will do the logic for saving a user whether it's a create or update, but also allow for async operation
func (client *natsUserClient) publishRequestEvent(user User, eventOpType common.EventType, waitForCompletion bool) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserClient.publishRequestEvent",
	})
	var u *UserModel = user.(*UserModel)
	logger.WithFields(log.Fields{
		"username":          u.Username,
		"eventOpType":       string(eventOpType),
		"waitForCompletion": waitForCompletion,
	}).Trace("publishRequestEvent() called")

	if u.Username == "" {
		return errors.New(UserUsernameNotSetError)
	}

	// let's set session actor and emulator from svc object
	u.SessionActor = client.actor
	u.SessionEmulator = client.actor

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	transactionID := messaging.NewTransactionID()

	if waitForCompletion {

		// note, since we need to handle a custom cancellation process, rather than
		// a simple cancel or timeout through context, let's just setup a context with
		// cancel enabled
		_, cancel := context.WithCancel(client.ctx)
		responseChannel := make(chan UserModel)
		defer close(responseChannel)

		handler := func(ev common.EventType, ce cloudevents.Event) {
			var eventUser UserModel
			err := json.Unmarshal(ce.Data(), &eventUser)
			if err != nil {
				errorMessage := "unable to unmarshal a user"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- UserModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}
			responseChannel <- eventUser
		}

		var listenerID messaging.ListenerID

		// listens to different event type depends on event operation
		switch eventOpType {
		case EventUserAddRequested:
			listenerID, err = client.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserAdded, EventUserAddError},
				transactionID, messaging.Listener{Callback: handler, ListenOnce: true})
		case EventUserUpdateRequested:
			listenerID, err = client.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserUpdated, EventUserUpdateError},
				transactionID, messaging.Listener{Callback: handler, ListenOnce: true})
		case EventUserDeleteRequested:
			listenerID, err = client.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserDeleted, EventUserDeleteError},
				transactionID, messaging.Listener{Callback: handler, ListenOnce: true})
		default:
			panic("unknown op") // unknown op must not be passed
		}
		if err != nil {
			cancel()
			return err
		}
		defer eventSource.RemoveListenerByID(listenerID)

		logger.Trace("sync publishing the event " + eventOpType)
		err = stanConn.PublishWithTransactionID(eventOpType, *u, transactionID)
		if err != nil {
			cancel()
			errorMessage := "unable to publish a update workspace event"
			logger.WithError(err).Error(errorMessage)
			return NewCacaoCommunicationError(errorMessage)
		}
		defer cancel()

		// wait for response
		responseReceived, response := waitUserResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
		if responseReceived {
			serviceError := response.GetServiceError()
			if serviceError == nil {
				return nil
			}

			logger.Error(serviceError)
			return serviceError
		}

		// timed out
		logger.Error("unable to create a workspace - workspace service is not responding")
		return NewCacaoTimeoutError("unable to create a workspace - workspace service is not responding")
		// }
	} else {
		logger.Trace("async publishing the event " + eventOpType)
		err = stanConn.PublishWithTransactionID(eventOpType, *u, transactionID)
		if err != nil {
			errorMessage := "unable to publish a update workspace event"
			logger.WithError(err).Error(errorMessage)
			return NewCacaoCommunicationError(errorMessage)
		}
	}
	return nil
}

// NewNatsUser creates a new NatsUser struct based on the nats connection; developers will still need
// to Load() after creating a new NatsUser
// actor is the username of the person making requests
// source is a string representing the source/service that is invoking the request (used for cloudevent)
// ctx can be nil, if one isn't available
func (client *natsUserClient) Get(username string) (User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserClient.Get",
	})
	logger.Trace("Get() started")

	if username == "" {
		return nil, errors.New(UserUsernameNotSetError)
	}

	logger.WithField("natsUrl", client.natsConfig.URL).Trace("connecting to nats")
	nc, err := nats.Connect(client.natsConfig.URL, nats.MaxReconnects(client.maxReconnect), nats.ReconnectWait(client.reconnectWait))
	if err != nil {
		return nil, err
	}
	defer nc.Close()

	logger.Trace("creating cloud event")
	user := UserModel{Session: Session{SessionActor: client.actor, SessionEmulator: client.emulator},
		Username: username}
	ce, err := messaging.CreateCloudEvent(user, NatsSubjectUsersGet, client.natsConfig.ClientID)
	if err != nil {
		return nil, err
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		return nil, err
	}

	// retrieve the loaded user from the nats ether
	logger.Trace("sending request with context to " + NatsSubjectUsersGet)
	newctx, cancel := context.WithTimeout(client.ctx, client.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, NatsSubjectUsersGet, payload)
	cancel() // no longer need the context, so cancel at this point
	if err != nil {
		return nil, err
	}

	// convert the message to a cloud event
	logger.Trace("received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(ce.Data(), &user)
	if err != nil {
		return nil, err
	}

	// final check for error
	if user.ErrorType != "" {
		logger.WithField("err", user.ErrorType).Trace("error was received")
		err = errors.New(user.ErrorType)
	}

	return &user, err
}

func (usersvc *natsUserClient) Delete(user User) error {
	log.Trace("natsUserClient.Delete() started")
	return usersvc.publishRequestEvent(user, EventUserDeleteRequested, true)
}

func (usersvc *natsUserClient) Login(user User) error {
	log.Trace("natsUserClient.Login() called")
	return usersvc.publishRequestEvent(user, EventUserLogin, false)
}

func (usersvc *natsUserClient) Search(filter UserListFilter) (UserList, error) {
	log.Trace("natsUserClient.Search() started")
	ul := &UserListModel{Session: Session{SessionActor: usersvc.actor, SessionEmulator: usersvc.emulator},
		Filter: filter}

	err := usersvc.load(ul)
	if err != nil {
		ul = nil
	}
	return ul, err
}

func (usersvc *natsUserClient) SearchNext(ul UserList) error {
	log.Trace("natsUserClient.SearchNext() started")

	var ulm *UserListModel = ul.(*UserListModel)

	if ulm.NextStart < 0 {
		return errors.New(UserListLoadBoundaryError)
	}

	// just set the filter index to the new starting point
	ulm.Filter.Start = ulm.NextStart

	return usersvc.load(ulm)
}

func (usersvc natsUserClient) load(ul *UserListModel) error {
	log.Trace("natsUserClient.load() started")

	// TODO: replace when implemented
	return errors.New(GeneralMethodNotYetImplementedError)
	/*
		if ul.Filter == (UserListFilter{}) { // compare with an uninitialized UserListFilter
			return errors.New(UserListFilterNotSetError)
		}
		if ul.Filter.Value == "" {
			log.Warning("NatsUserList.Load: " + UserListFilterValueEmptyWarning)
		}
		if ul.Filter.MaxItems < 1 {
			return errors.New(UserListFilterInvalidMaxItemsError)
		}
		if ul.Filter.Start < 0 {
			return errors.New(UserListFilterInvalidStartIndexError)
		}
		if ul.Filter.SortBy == 0 { // default sort
			ul.Filter.SortBy = AscendingSort
		}

		nc, err := nats.Connect(usersvc.natsUrl, nats.MaxReconnects(messaging.DefaultNatsMaxReconnect), nats.ReconnectWait(messaging.DefaultNatsReconnectWait))
		if err != nil {
			return err
		}
		defer nc.Close()

		ce, err := messaging.CreateCloudEvent(ul, NatsSubjectUsersList, usersvc.clientID)
		if err != nil {
			return err
		}
		payload, err := json.Marshal(ce)

		if err != nil {
			return err
		}

		// retrieve the loaded user from the nats ether
		newctx, cancel := context.WithTimeout(usersvc.ctx, usersvc.requestTimeout)
		msg, err := nc.RequestWithContext(newctx, NatsSubjectUsersList, payload)
		cancel() // no longer need the context, so cancel at this point
		if err != nil {
			return err
		}

		// convert the message to a cloud event
		ce, err = messaging.ConvertNats(msg)
		if err != nil {
			return err
		}

		err = json.Unmarshal(ce.Data(), &ul)
		if err != nil {
			return err
		}

		return nil
	*/
}
