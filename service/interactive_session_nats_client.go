package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"net/http"
	"time"
)

const (
	InteractiveSessionsListQueryOp  common.QueryOp = "cyverse.isms.list"
	InteractiveSessionsCheckQueryOp common.QueryOp = "cyverse.isms.check"
)

const (
	GuacamoleAddRequestedEvent common.EventType = "GuacamoleCreateRequested"
	GuacamoleAddedEvent        common.EventType = "GuacamoleAdded"
	GuacamoleAddFailedEvent    common.EventType = "GuacamoleAddFailed"
)

type natsInteractiveSessionClient struct {
	actor          string
	emulator       string
	clientID       string
	natsUrl        string
	clusterID      string
	natsConfig     messaging.NatsConfig
	stanConfig     messaging.StanConfig
	maxReconnect   int
	reconnectWait  time.Duration
	requestTimeout time.Duration
	eventsChannel  string
	eventsTimeout  time.Duration
	ctx            context.Context
}

// NewNatsInteractiveSessionClient ...
//  @Description: Nats Implementation of the interactive session client. Requires standard nats configuration.
//	Implements the Create, Check and List functions found the InteractiveSession model.
func NewNatsInteractiveSessionClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (InteractiveSessionClient, error) {
	log.Trace("NewNatsInteractiveSessionClient() called")

	if actor == "" {
		return nil, errors.New("actor Not Set")
	}

	if ctx == nil {
		ctx = context.Background()
	}
	newInteractiveSessionSvc := natsInteractiveSessionClient{
		actor:          actor,
		emulator:       emulator,
		natsUrl:        natsConfig.URL,
		clientID:       natsConfig.ClientID,
		clusterID:      stanConfig.ClusterID,
		eventsChannel:  common.EventsSubject,
		natsConfig:     natsConfig,
		stanConfig:     stanConfig,
		requestTimeout: 10,
		ctx:            ctx}

	// for _, option := range options { // call the options
	// 	option(&new_user_svc)
	// }

	if natsConfig.MaxReconnects <= 0 {
		newInteractiveSessionSvc.maxReconnect = messaging.DefaultNatsMaxReconnect
	} else {
		newInteractiveSessionSvc.maxReconnect = natsConfig.MaxReconnects
	}
	if natsConfig.ReconnectWait <= 0 {
		newInteractiveSessionSvc.reconnectWait = messaging.DefaultNatsReconnectWait
	} else {
		newInteractiveSessionSvc.reconnectWait = time.Duration(natsConfig.ReconnectWait) * time.Second
	}
	if natsConfig.RequestTimeout <= 0 {
		newInteractiveSessionSvc.requestTimeout = messaging.DefaultNatsRequestTimeout
	} else {
		newInteractiveSessionSvc.requestTimeout = time.Duration(natsConfig.RequestTimeout) * time.Second
	}

	return &newInteractiveSessionSvc, nil
}

func waitInteractiveSessionResponse(c chan InteractiveSessionModel, timeout time.Duration) (bool, InteractiveSessionModel) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, InteractiveSessionModel{}
	}
}

// Create ...
//  @Description: NATS Implementation of the Create function for the ISMS. Please see interactive_session.go for
//	Additional information.
func (issvc *natsInteractiveSessionClient) Create(is InteractiveSession) (InteractiveSession, error) {

	log.Trace("natsInteractiveSessionClient.Create() started")

	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.Create",
	})

	valid, verr := is.Verify()

	if !valid || verr != nil {
		logger.WithFields(log.Fields{"error": verr}).Error("Unable to verify provided IS")
		return nil, verr
	}

	SenderNatsConfig := issvc.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &issvc.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return nil, err
	}
	defer stanConn.Disconnect()

	eventSourceNatsConfig := issvc.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, issvc.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return nil, err
	}

	request := is

	eventsSubscribe := []common.EventType{
		GuacamoleAddedEvent,
		GuacamoleAddFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan InteractiveSessionModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == GuacamoleAddedEvent || ev == GuacamoleAddFailedEvent {
			var is InteractiveSessionModel
			err := json.Unmarshal(ce.Data(), &is)
			log.WithFields(log.Fields{"request": string(ce.Data())}).Trace("received message")

			if err != nil {
				logger.WithError(err).Error("unable to unmarshal an IS create response")
				responseChannel <- InteractiveSessionModel{
					Session: Session{
						ErrorType:    string(rune(http.StatusInternalServerError)),
						ErrorMessage: "unable to unmarshal a Ineractive-Session create response",
					},
				}
				return
			}
			errType, errMsg := is.GetError()
			if errType != "" {
				responseChannel <- InteractiveSessionModel{
					Session: Session{
						ErrorType:    errType,
						ErrorMessage: errMsg,
					},
				}
				return
			}
			responseChannel <- is
			return
		}

		responseChannel <- InteractiveSessionModel{
			Session: Session{
				ErrorType:    "Unknown Event Type",
				ErrorMessage: "Unknown Event Error",
			},
		}
		return
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithError(err).Error("unable to add an event listener")
		return nil, err
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(GuacamoleAddRequestedEvent, request, transactionID)
	if err != nil {
		logger.WithError(err).Error("unable to publish a create Interactive-Session event")
		return nil, err
	}

	responseReceived, response := waitInteractiveSessionResponse(responseChannel, issvc.requestTimeout*time.Second)
	if responseReceived {

		err, _ := response.GetError()

		if err == "" {
			log.Error(response.Protocol)
			log.Error(response.InstanceID)
			return &response, nil
		}

		logger.Error(err)
		return nil, errors.New(err)
	}

	// timed out
	err = fmt.Errorf("unable to create a Interactive-Session - Interactive-Session service is not responding")
	logger.WithError(err).Error("timeout")
	return nil, err
}

// Check ...
//  @Description: NATS Implementation of the Check function for the ISMS. Please see interactive_session.go for
//	Additional information.
func (issvc *natsInteractiveSessionClient) Check(is InteractiveSession) (bool, error) {
	log.Trace("natsInteractiveSessionClient.Check Called")

	nc, err := nats.Connect(issvc.natsUrl, nats.MaxReconnects(issvc.maxReconnect), nats.ReconnectWait(issvc.reconnectWait))
	if err != nil {
		return false, err
	}
	defer nc.Close()

	ce, err := messaging.CreateCloudEvent(is, string(InteractiveSessionsCheckQueryOp), issvc.clientID)

	if err != nil {
		return false, err
	}

	bytes, err := json.Marshal(ce)

	if err != nil {
		return false, err
	}

	newctx, cancel := context.WithTimeout(issvc.ctx, issvc.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, string(InteractiveSessionsCheckQueryOp), bytes)
	cancel()

	if err != nil {
		return false, err
	}

	// convert the message to a cloud event
	log.Trace("natsInteractiveSessionClient.Check: received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return false, err
	}
	log.WithFields(log.Fields{"request": string(ce.Data())}).Trace("received message")

	var ses InteractiveSessionModel

	err = json.Unmarshal(ce.Data(), &ses)
	if err != nil {
		return false, err
	}

	if ses.Error != "" {
		return false, errors.New(ses.Error)
	}

	return true, nil
}

// List ...
//  @Description: NATS Implementation of the List function for the ISMS. Please see interactive_session.go for
//	Additional information.
func (issvc *natsInteractiveSessionClient) List(is InteractiveSession) ([]InteractiveSession, error) {
	log.Trace("natsInteractiveSessionClient.List Called")

	nc, err := nats.Connect(issvc.natsUrl, nats.MaxReconnects(issvc.maxReconnect), nats.ReconnectWait(issvc.reconnectWait))
	if err != nil {
		return nil, err
	}
	defer nc.Close()

	ce, err := messaging.CreateCloudEvent(is, string(InteractiveSessionsListQueryOp), issvc.clientID)

	if err != nil {
		return nil, err
	}

	bytes, err := json.Marshal(ce)

	if err != nil {
		return nil, err
	}

	newctx, cancel := context.WithTimeout(issvc.ctx, issvc.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, string(InteractiveSessionsListQueryOp), bytes)
	cancel()

	if err != nil {
		return nil, err
	}

	// convert the message to a cloud event
	log.Trace("natsInteractiveSessionClient.List: received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return nil, err
	}
	log.WithFields(log.Fields{"request": string(ce.Data())}).Trace("received message")

	var list []InteractiveSessionModel
	var credslist []InteractiveSession

	err = json.Unmarshal(ce.Data(), &list)
	if err != nil {
		return nil, err
	}

	for i := range list {
		credslist = append(credslist, &list[i])
	}

	return credslist, err
}
