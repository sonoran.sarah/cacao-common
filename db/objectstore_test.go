// Package db contains utility functions for dealing with database
package db

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

type testobject struct {
	ID        string `bson:"_id" json:"id,omitempty"`
	Owner     string `bson:"owner" json:"owner,omitempty"`
	Field     string `bson:"field" json:"field,omitempty"`
	JSONField string `json:"json_field,omitempty"`
}

type testobjectcomplex struct {
	ID        string                 `bson:"_id" json:"id,omitempty"`
	Owner     string                 `bson:"owner" json:"owner,omitempty"`
	Testfloat float32                `bson:"testfloat" json:"testfloat,omitempty"`
	Testobj   testobject             `bson:"testobj" json:"testobj,omitempty"`
	Testmap   map[string]interface{} `bson:"testmap" json:"testmap,omitempty"`
	Testarr   []string               `bson:"testarr" json:"testarr,omitempty"`
	Testarr2  []testobject           `bson:"testarr2" json:"testarr2,omitempty"`
	Testtime  time.Time              `bson:"testtime" json:"testtime,omitempty"`
}

func TestCreateObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)
}

func TestReleaseObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	err = store.Release()
	assert.NoError(t, err)
	assert.Nil(t, store.Mock)
}

func TestListObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			ID:    "0001",
			Owner: "test_owner1",
			Field: "test_field1",
		},
		{
			ID:    "0002",
			Owner: "test_owner2",
			Field: "test_field2",
		},
	}
	store.Mock.On("List", "test_collection").Return(expectedResults, nil)

	results := []testobject{}
	err = store.List("test_collection", &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestListConditionalObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			ID:    "0001",
			Owner: "test_owner1",
			Field: "test_field1",
		},
		{
			ID:    "0002",
			Owner: "test_owner2",
			Field: "test_field2",
		},
	}
	store.Mock.On("ListConditional", "test_collection", map[string]interface{}{}).Return(expectedResults, nil)

	results := []testobject{}
	err = store.ListConditional("test_collection", bson.M{}, &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestListConditionalObjectStore2(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			ID:    "0001",
			Owner: "test_owner1",
			Field: "test_field1",
		},
	}
	store.Mock.On("ListConditional", "test_collection", map[string]interface{}{"id": "0001"}).Return(expectedResults, nil)

	results := []testobject{}
	err = store.ListConditional("test_collection", bson.M{"id": "0001"}, &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestListForUserObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			ID:    "0001",
			Owner: "test_owner1",
			Field: "test_field1",
		},
		{
			ID:    "0002",
			Owner: "test_owner1",
			Field: "test_field2",
		},
	}
	store.Mock.On("ListForUser", "test_collection", "test_owner1").Return(expectedResults, nil)

	results := []testobject{}
	err = store.ListForUser("test_collection", "test_owner1", &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestGetConditionalObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResult := testobject{
		ID:    "0001",
		Owner: "test_owner1",
		Field: "test_field1",
	}
	store.Mock.On("GetConditional", "test_collection", map[string]interface{}{"id": "0001"}).Return(expectedResult, nil)

	result := testobject{}
	err = store.GetConditional("test_collection", bson.M{"id": "0001"}, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, &result)
	assert.Equal(t, result.Owner, "test_owner1")

	err = store.Release()
	assert.NoError(t, err)
}

func TestGetObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResult := testobject{
		ID:    "0001",
		Owner: "test_owner1",
		Field: "test_field1",
	}
	store.Mock.On("Get", "test_collection", "0001").Return(expectedResult, nil)

	result := testobject{}
	err = store.Get("test_collection", "0001", &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, &result)
	assert.Equal(t, result.Owner, "test_owner1")

	err = store.Release()
	assert.NoError(t, err)
}

func TestInsertObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	insertObject := testobject{
		ID:    "0001",
		Owner: "test_owner1",
		Field: "test_field1",
	}
	store.Mock.On("Insert", "test_collection").Return(nil)

	err = store.Insert("test_collection", insertObject)
	assert.NoError(t, err)

	err = store.Release()
	assert.NoError(t, err)
}

func TestInsertManyObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	insertObjects := []interface{}{
		testobject{
			ID:    "0001",
			Owner: "test_owner1",
			Field: "test_field1",
		},
		testobject{
			ID:    "0002",
			Owner: "test_owner2",
			Field: "test_field2",
		},
	}

	store.Mock.On("InsertMany", "test_collection").Return(nil)

	err = store.InsertMany("test_collection", insertObjects)
	assert.NoError(t, err)

	err = store.Release()
	assert.NoError(t, err)
}

func TestUpdateObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	updateObject := testobject{
		ID:    "0001",
		Owner: "test_owner1",
		Field: "test_field1",
	}
	store.Mock.On("Update", "test_collection", "0001").Return(true, nil)

	result, err := store.Update("test_collection", "0001", updateObject, []string{
		"owner", "field",
	})
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestUpdateObjectStoreComplexType(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	updateObject := testobjectcomplex{
		ID:        "0001",
		Owner:     "test_owner1",
		Testfloat: 0.3233133,
		Testobj: testobject{
			ID:        "0001",
			Owner:     "test_owner1",
			Field:     "test_field1",
			JSONField: "test_json_field1",
		},
		Testmap: map[string]interface{}{
			"f1": 3133234,
			"f2": "string test",
			"f3": true,
		},
		Testarr: []string{
			"a", "b", "c",
		},
		Testarr2: []testobject{
			{
				ID:    "0001",
				Owner: "test_owner1",
				Field: "test_field1",
			},
			{
				ID:    "0002",
				Owner: "test_owner2",
			},
			{
				ID:    "0003",
				Owner: "test_owner3",
				Field: "",
			},
		},
		Testtime: time.Now(),
	}
	store.Mock.On("Update", "test_collection", "0001").Return(true, nil)

	result, err := store.Update("test_collection", "0001", updateObject, []string{
		"testfloat", "testobj", "testmap", "testarr2",
	})
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestUpdateWithMapObjectStoreComplexType(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	updateObject := map[string]interface{}{
		"owner":     "test_owner1",
		"testfloat": 0.3233133,
		"testobj": testobject{
			ID:        "0001",
			Owner:     "test_owner1",
			Field:     "test_field1",
			JSONField: "test_jsonfield1",
		},
		"testmap": map[string]interface{}{
			"f1": 3133234,
			"f2": "string test",
			"f3": true,
		},
		"testarr": []string{
			"a", "b", "c",
		},
		"testarr2": []testobject{
			{
				ID:    "0001",
				Owner: "test_owner1",
				Field: "test_field1",
			},
			{
				ID:    "0002",
				Owner: "test_owner2",
			},
			{
				ID:    "0003",
				Owner: "test_owner3",
				Field: "",
			},
		},
		"testtime": time.Now(),
	}
	store.Mock.On("UpdateWithMap", "test_collection", "0001").Return(true, nil)

	result, err := store.UpdateWithMap("test_collection", "0001", updateObject)
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestReplaceObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	replaceObject := testobject{
		ID:    "0001",
		Owner: "test_owner1",
		Field: "test_field1_updated",
	}
	store.Mock.On("Replace", "test_collection", "0001").Return(true, nil)

	result, err := store.Replace("test_collection", "0001", replaceObject)
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestDeleteObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	store.Mock.On("Delete", "test_collection", "0001").Return(true, nil)

	result, err := store.Delete("test_collection", "0001")
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestDeleteConditionalObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	store.Mock.On("DeleteConditional", "test_collection", map[string]interface{}{"id": "0001"}).Return(1, nil)

	result, err := store.DeleteConditional("test_collection", bson.M{"id": "0001"})
	assert.NoError(t, err)
	assert.Equal(t, result, int64(1))

	err = store.Release()
	assert.NoError(t, err)
}
