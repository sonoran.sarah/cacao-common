// Package db contains utility functions for dealing with database
package db

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
)

// MongoDBObjectStore that provides object level access to MongoDB
// implements ObjectStore interface
type MongoDBObjectStore struct {
	Connection *MongoDBConnection
}

// CreateMongoDBObjectStore connects to MongoDB and returns MongoDBObjectStore
func CreateMongoDBObjectStore(config *MongoDBConfig) (*MongoDBObjectStore, error) {
	conn, err := NewMongoDBConnection(config)
	if err != nil {
		return nil, err
	}

	return &MongoDBObjectStore{
		Connection: conn,
	}, nil
}

// Release disconnects MongoDB connection and realses resources
func (store *MongoDBObjectStore) Release() error {
	err := store.Connection.Disconnect()
	if err != nil {
		return err
	}

	store.Connection = nil
	return nil
}

// List lists all documents in the given collection
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (store *MongoDBObjectStore) List(collection string, results interface{}) error {
	return store.Connection.List(collection, bson.M{}, results)
}

// ListConditional lists documents in the given collection that satisfie the given conditions
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (store *MongoDBObjectStore) ListConditional(collection string, conditions map[string]interface{}, results interface{}) error {
	return store.Connection.List(collection, conditions, results)
}

// ListForUser lists all documents in the given collection for a user
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (store *MongoDBObjectStore) ListForUser(collection string, owner string, results interface{}) error {
	return store.Connection.List(collection, bson.M{"owner": owner}, results)
}

// GetConditional returns a document in the given collection that satisfies the given conditions
// results must be a pointer to a struct (i.e., *ExampleStruct)
func (store *MongoDBObjectStore) GetConditional(collection string, conditions map[string]interface{}, result interface{}) error {
	return store.Connection.Get(collection, conditions, result)
}

// Get returns a document in the given collection with the given id
// results must be a pointer to a struct (i.e., *ExampleStruct)
func (store *MongoDBObjectStore) Get(collection string, id string, result interface{}) error {
	return store.Connection.Get(collection, bson.M{"_id": id}, result)
}

// Insert inserts a document into the given collection
func (store *MongoDBObjectStore) Insert(collection string, document interface{}) error {
	if !validateDocumentType(document) {
		return fmt.Errorf("cannot find mandatory fields in the given docuemnt")
	}

	return store.Connection.Insert(collection, document)
}

// InsertMany inserts multiple documents into the given collection
func (store *MongoDBObjectStore) InsertMany(collection string, documents []interface{}) error {
	for _, document := range documents {
		if !validateDocumentType(document) {
			return fmt.Errorf("cannot find mandatory fields in one of the given docuemnts")
		}
	}

	return store.Connection.InsertMany(collection, documents)
}

// Update updates a document in the given collection, returns true if updated successfully
func (store *MongoDBObjectStore) Update(collection string, id string, updateDocument interface{}, updateFieldNames []string) (bool, error) {
	if !validateDocumentType(updateDocument) {
		return false, fmt.Errorf("cannot find mandatory fields in the given docuemnt")
	}

	updateFields := extractFieldsToUpdate(updateDocument, updateFieldNames, false, true)
	if len(updateFields) == 0 {
		return false, fmt.Errorf("cannot find fields to update in the given docuemnt")
	}

	return store.Connection.Update(collection, bson.M{"_id": id}, bson.M{"$set": updateFields})
}

// UpdateWithMap updates a document in the given collection, returns true if updated successfully
func (store *MongoDBObjectStore) UpdateWithMap(collection string, id string, updateDocument map[string]interface{}) (bool, error) {
	if len(updateDocument) == 0 {
		return false, fmt.Errorf("cannot find fields to update in the given docuemnt")
	}

	return store.Connection.Update(collection, bson.M{"_id": id}, bson.M{"$set": updateDocument})
}

// Replace replaces a document in the given collection, returns true if replaced successfully
// Replace is different from Update. It updates an entire document, while Update updates some fields in a document.
func (store *MongoDBObjectStore) Replace(collection string, id string, replaceDocument interface{}) (bool, error) {
	if !validateDocumentType(replaceDocument) {
		return false, fmt.Errorf("cannot find mandatory fields in the given docuemnt")
	}

	return store.Connection.Replace(collection, bson.M{"_id": id}, replaceDocument)
}

// Delete deletes a document in the given collection, returns true if deleted successfully
func (store *MongoDBObjectStore) Delete(collection string, id string) (bool, error) {
	return store.Connection.Delete(collection, bson.M{"_id": id})
}

// DeleteConditional deletes multiple document in the given collection that satisfy the given conditions, returns the number of documents deleted as the first result
func (store *MongoDBObjectStore) DeleteConditional(collection string, conditions map[string]interface{}) (int64, error) {
	return store.Connection.DeleteMany(collection, conditions)
}
