// Package db contains utility functions for dealing with database
package db

import (
	"fmt"
	"reflect"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
)

// ObjectStore is an interface for object-level database I/O
type ObjectStore interface {
	Release() error
	List(collection string, results interface{}) error
	ListConditional(collection string, conditions map[string]interface{}, results interface{}) error
	ListForUser(collection string, owner string, results interface{}) error
	Get(collection string, id string, result interface{}) error
	GetConditional(collection string, conditions map[string]interface{}, result interface{}) error
	Insert(collection string, document interface{}) error
	InsertMany(collection string, documents []interface{}) error
	Update(collection string, id string, updateDocument interface{}, updateFieldNames []string) (bool, error)
	UpdateWithMap(collection string, id string, updateDocument map[string]interface{}) (bool, error)
	Replace(collection string, id string, replaceDocument interface{}) (bool, error)
	Delete(collection string, id string) (bool, error)
	DeleteConditional(collection string, conditions map[string]interface{}) (int64, error)
}

// validateDocumentType checks docuement type if it has mandatory fields, such as _id
// The document must contain following bson fields
// _id: the ID of a document
func validateDocumentType(document interface{}) bool {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "validateDocumentType",
	})

	documentType := reflect.TypeOf(document)
	if documentType.Kind() != reflect.Struct {
		return false
	}

	hasIDField := false

	for i := 0; i < documentType.NumField(); i++ {
		fieldTag := documentType.Field(i).Tag
		if tagValue, ok := fieldTag.Lookup("bson"); ok {
			tagValues := strings.Split(tagValue, ",")
			bsonFieldName := ""
			if len(tagValues) > 0 {
				bsonFieldName = tagValues[0]
			}

			if bsonFieldName == "_id" {
				// _id exists
				hasIDField = true
			}

			switch bsonFieldName {
			case "_id":
				// _id exists
				hasIDField = true
			default:
				// do nothing
			}
		}
	}

	if !hasIDField {
		logger.Tracef("cannot find _id field in a docuemnt %s", documentType.Name())
	}

	return hasIDField
}

// extractFieldsToUpdate extracts fields to update from a docuement
func extractFieldsToUpdate(document interface{}, updateFieldNames []string, updateAllFields bool, filterIDField bool) bson.M {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "extractFieldsToUpdate",
	})

	updateFields := bson.M{}

	documentValue := reflect.ValueOf(document)
	if documentValue.Kind() == reflect.Ptr {
		documentValue = documentValue.Elem()
	}
	documentType := reflect.TypeOf(document)
	if documentType.Kind() == reflect.Ptr {
		documentType = documentType.Elem()
	}

	updateFieldNamesMap := map[string]bool{}
	for _, name := range updateFieldNames {
		updateFieldNamesMap[name] = true
	}

	for i := 0; i < documentType.NumField(); i++ {
		fieldTag := documentType.Field(i).Tag
		tagValue := ""
		if bsonTagValue, ok := fieldTag.Lookup("bson"); ok {
			// use bson first
			tagValue = bsonTagValue
		} else if jsonTagValue, ok := fieldTag.Lookup("json"); ok {
			// if bson not exist, use json
			tagValue = jsonTagValue
		}

		if len(tagValue) > 0 {
			tagValues := strings.Split(tagValue, ",")
			fieldName := ""
			if len(tagValues) > 0 {
				fieldName = tagValues[0]
			}

			if filterIDField && fieldName == "_id" {
				// filter
				continue
			} else if fieldName == "-" {
				// ignore
				continue
			} else if len(fieldName) > 0 {
				_, updateField := updateFieldNamesMap[fieldName]
				if updateField || updateAllFields {
					fieldValue := documentValue.Field(i)
					val, err := castReflectValueToValue(fieldValue)
					if err != nil {
						logger.Error(err)
					} else {
						updateFields[fieldName] = val
					}
				}
			}
		}
	}

	return updateFields
}

// castReflectValueToValue casts reflect.Value to real data type
func castReflectValueToValue(v reflect.Value) (interface{}, error) {
	switch v.Kind() {
	case reflect.Bool:
		return v.Bool(), nil
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int(), nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return v.Uint(), nil
	case reflect.Float32, reflect.Float64:
		return v.Float(), nil
	case reflect.String:
		return v.String(), nil
	case reflect.Interface:
		return v.Interface(), nil
	case reflect.Struct:
		if v.Type() == reflect.TypeOf(time.Time{}) {
			// handle time.Time
			return v.Interface().(time.Time), nil
		}
		return extractFieldsToUpdate(v.Interface(), nil, true, false), nil
	case reflect.Map:
		return v.Interface(), nil
	case reflect.Slice, reflect.Array:
		arr := []interface{}{}
		for i := 0; i < v.Len(); i++ {
			entry := v.Index(i)
			entryVal, err := castReflectValueToValue(entry)
			if err != nil {
				return nil, err
			}
			arr = append(arr, entryVal)
		}
		return arr, nil
	case reflect.Ptr:
		return v.Elem().Interface(), nil
	default:
		return nil, fmt.Errorf("unknown type")
	}
}
