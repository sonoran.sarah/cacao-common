// Package db contains utility functions for dealing with database
package db

import (
	"fmt"
	"reflect"

	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
)

// MockObjectStore mocks object store
// implements ObjectStore interface
type MockObjectStore struct {
	Mock *mock.Mock
}

// CreateMockObjectStore creates MockObjectStore
func CreateMockObjectStore() (*MockObjectStore, error) {
	return &MockObjectStore{
		Mock: new(mock.Mock),
	}, nil
}

// Release realses resources
func (store *MockObjectStore) Release() error {
	store.Mock = nil
	return nil
}

func addToSlice(sliceVal reflect.Value, elemType reflect.Type, elements []interface{}) (reflect.Value, int) {
	index := 0
	for _, element := range elements {
		if sliceVal.Len() == index {
			// slice is full
			newElem := reflect.New(elemType)
			sliceVal = reflect.Append(sliceVal, newElem.Elem())
			sliceVal = sliceVal.Slice(0, sliceVal.Cap())
		}

		elementValue := reflect.ValueOf(element)
		sliceVal.Index(index).Set(elementValue)

		index++
	}
	return sliceVal, index
}

func convertInterfaceToSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		return nil
	}

	if s.IsNil() {
		return nil
	}

	ret := make([]interface{}, s.Len())

	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret
}

// List lists all documents in the given collection
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (store *MockObjectStore) List(collection string, results interface{}) error {
	resultsVal := reflect.ValueOf(results)
	if resultsVal.Kind() != reflect.Ptr {
		return fmt.Errorf("results argument must be a pointer to a slice, but was a %s", resultsVal.Kind())
	}

	sliceVal := resultsVal.Elem()
	if sliceVal.Kind() == reflect.Interface {
		sliceVal = sliceVal.Elem()
	}

	if sliceVal.Kind() != reflect.Slice {
		return fmt.Errorf("results argument must be a pointer to a slice, but was a pointer to %s", sliceVal.Kind())
	}

	args := store.Mock.Called(collection)

	expectedResults := convertInterfaceToSlice(args.Get(0))

	resultType := reflect.TypeOf(expectedResults[0])
	sliceVal, index := addToSlice(sliceVal, resultType, expectedResults)

	resultsVal.Elem().Set(sliceVal.Slice(0, index))

	return args.Error(1)
}

// ListConditional lists documents in the given collection that satisfie the given conditions
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (store *MockObjectStore) ListConditional(collection string, conditions map[string]interface{}, results interface{}) error {
	resultsVal := reflect.ValueOf(results)
	if resultsVal.Kind() != reflect.Ptr {
		return fmt.Errorf("results argument must be a pointer to a slice, but was a %s", resultsVal.Kind())
	}

	sliceVal := resultsVal.Elem()
	if sliceVal.Kind() == reflect.Interface {
		sliceVal = sliceVal.Elem()
	}

	if sliceVal.Kind() != reflect.Slice {
		return fmt.Errorf("results argument must be a pointer to a slice, but was a pointer to %s", sliceVal.Kind())
	}

	args := store.Mock.Called(collection, conditions)

	expectedResults := convertInterfaceToSlice(args.Get(0))

	resultType := reflect.TypeOf(expectedResults[0])
	sliceVal, index := addToSlice(sliceVal, resultType, expectedResults)

	resultsVal.Elem().Set(sliceVal.Slice(0, index))

	return args.Error(1)
}

// ListForUser lists all documents in the given collection for a user
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (store *MockObjectStore) ListForUser(collection string, owner string, results interface{}) error {
	resultsVal := reflect.ValueOf(results)
	if resultsVal.Kind() != reflect.Ptr {
		return fmt.Errorf("results argument must be a pointer to a slice, but was a %s", resultsVal.Kind())
	}

	sliceVal := resultsVal.Elem()
	if sliceVal.Kind() == reflect.Interface {
		sliceVal = sliceVal.Elem()
	}

	if sliceVal.Kind() != reflect.Slice {
		return fmt.Errorf("results argument must be a pointer to a slice, but was a pointer to %s", sliceVal.Kind())
	}

	args := store.Mock.Called(collection, owner)

	expectedResults := convertInterfaceToSlice(args.Get(0))

	resultType := reflect.TypeOf(expectedResults[0])
	sliceVal, index := addToSlice(sliceVal, resultType, expectedResults)

	resultsVal.Elem().Set(sliceVal.Slice(0, index))

	return args.Error(1)
}

// GetConditional returns a document in the given collection that satisfies the given conditions
// results must be a pointer to a struct (i.e., *ExampleStruct)
func (store *MockObjectStore) GetConditional(collection string, conditions map[string]interface{}, result interface{}) error {
	resultVal := reflect.ValueOf(result)
	if resultVal.Kind() != reflect.Ptr {
		return fmt.Errorf("results argument must be a pointer to a struct, but was a %s", resultVal.Kind())
	}

	resultItemVal := resultVal.Elem()
	if resultItemVal.Kind() == reflect.Interface {
		resultItemVal = resultItemVal.Elem()
	}

	if resultItemVal.Kind() != reflect.Struct {
		return fmt.Errorf("results argument must be a pointer to a struct, but was a pointer to %s", resultItemVal.Kind())
	}

	args := store.Mock.Called(collection, conditions)

	expectedResult := args.Get(0)

	expectedResultVal := reflect.ValueOf(expectedResult)
	resultVal.Elem().Set(expectedResultVal)

	return args.Error(1)
}

// Get returns a document in the given collection with the given id
// results must be a pointer to a struct (i.e., *ExampleStruct)
func (store *MockObjectStore) Get(collection string, id string, result interface{}) error {
	resultVal := reflect.ValueOf(result)
	if resultVal.Kind() != reflect.Ptr {
		return fmt.Errorf("results argument must be a pointer to a struct, but was a %s", resultVal.Kind())
	}

	resultItemVal := resultVal.Elem()
	if resultItemVal.Kind() == reflect.Interface {
		resultItemVal = resultItemVal.Elem()
	}

	if resultItemVal.Kind() != reflect.Struct {
		return fmt.Errorf("results argument must be a pointer to a struct, but was a pointer to %s", resultItemVal.Kind())
	}

	args := store.Mock.Called(collection, id)

	expectedResult := args.Get(0)

	expectedResultVal := reflect.ValueOf(expectedResult)
	resultVal.Elem().Set(expectedResultVal)

	return args.Error(1)
}

// Insert inserts a document into the given collection
func (store *MockObjectStore) Insert(collection string, document interface{}) error {
	if !validateDocumentType(document) {
		return fmt.Errorf("cannot find mandatory fields in the given docuemnt")
	}

	args := store.Mock.Called(collection)
	return args.Error(0)
}

// InsertMany inserts documents into the given collection
func (store *MockObjectStore) InsertMany(collection string, documents []interface{}) error {
	for _, document := range documents {
		if !validateDocumentType(document) {
			return fmt.Errorf("cannot find mandatory fields in one of the given docuemnts")
		}
	}

	args := store.Mock.Called(collection)
	return args.Error(0)
}

// Update updates a document in the given collection, returns true if updated successfully
func (store *MockObjectStore) Update(collection string, id string, updateDocument interface{}, updateFieldNames []string) (bool, error) {
	if !validateDocumentType(updateDocument) {
		return false, fmt.Errorf("cannot find mandatory fields in the given docuemnt")
	}

	updateFields := extractFieldsToUpdate(updateDocument, updateFieldNames, false, true)
	if len(updateFields) == 0 {
		return false, fmt.Errorf("cannot find fields to update in the given docuemnt")
	}

	_, err := bson.Marshal(updateFields)
	if err != nil {
		return false, err
	}

	args := store.Mock.Called(collection, id)
	return args.Bool(0), args.Error(1)
}

// UpdateWithMap updates a document in the given collection, returns true if updated successfully
func (store *MockObjectStore) UpdateWithMap(collection string, id string, updateDocument map[string]interface{}) (bool, error) {
	if len(updateDocument) == 0 {
		return false, fmt.Errorf("cannot find fields to update in the given docuemnt")
	}

	_, err := bson.Marshal(updateDocument)
	if err != nil {
		return false, err
	}

	args := store.Mock.Called(collection, id)
	return args.Bool(0), args.Error(1)
}

// Replace replaces a document in the given collection, returns true if replaced successfully
// Replace is different from Update. It updates an entire document, while Update updates some fields in a document.
func (store *MockObjectStore) Replace(collection string, id string, replaceDocument interface{}) (bool, error) {
	if !validateDocumentType(replaceDocument) {
		return false, fmt.Errorf("cannot find mandatory fields in the given docuemnt")
	}

	args := store.Mock.Called(collection, id)
	return args.Bool(0), args.Error(1)
}

// Delete deletes a document in the given collection, returns true if deleted successfully
func (store *MockObjectStore) Delete(collection string, id string) (bool, error) {
	args := store.Mock.Called(collection, id)
	return args.Bool(0), args.Error(1)
}

// DeleteConditional deletes multiple document in the given collection that satisfy the given conditions, returns the number of documents deleted as the first result
func (store *MockObjectStore) DeleteConditional(collection string, conditions map[string]interface{}) (int64, error) {
	args := store.Mock.Called(collection, conditions)
	return int64(args.Int(0)), args.Error(1)
}
