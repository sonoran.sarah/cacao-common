// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"context"
	"fmt"
	"sync"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	nats "github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

const (
	// DefaultNatsURL is a default nats URL
	DefaultNatsURL = "nats://nats:4222"
	// DefaultNatsClusterID is a default Cluster ID for cacao
	DefaultNatsClusterID = "cacao-cluster"
	// DefaultNatsMaxReconnect is default max reconnect trials
	DefaultNatsMaxReconnect = 6 // max times to reconnect within nats.connect()
	// DefaultNatsReconnectWait is a default delay for next reconnect
	DefaultNatsReconnectWait = 10 * time.Second // seconds to wait within nats.connect()
	// DefaultNatsRequestTimeout is default timeout for requests
	DefaultNatsRequestTimeout = 5 * time.Second // timeout for requests
)

// NatsConfig stores configurations used by both nats query channel and nats streaming
type NatsConfig struct {
	URL             string `envconfig:"NATS_URL" default:"nats://nats:4222"`
	QueueGroup      string `envconfig:"NATS_QUEUE_GROUP"`
	WildcardSubject string `envconfig:"NATS_WILDCARD_SUBJECT" default:"cyverse.>"` // WildcardSubject field is optional, only used for NATS Query
	ClientID        string `envconfig:"NATS_CLIENT_ID"`                            // While not strictly used by Nats, this is propagated into the cloudevent
	MaxReconnects   int    `envconfig:"NATS_MAX_RECONNECTS" default:"-1"`          // implementation should default to DefaultNatsMaxReconnect
	ReconnectWait   int    `envconfig:"NATS_RECONNECT_WAIT" default:"-1"`          // in seconds, implementation should default to
	RequestTimeout  int    `envconfig:"NATS_REQUEST_TIMEOUT" default:"-1"`
}

// NatsConnection contains Nats connection info
type NatsConnection struct {
	Config                   *NatsConfig
	Connection               *nats.Conn
	Subscription             *nats.Subscription
	HandlerLock              sync.Mutex
	EventHandlers            map[common.QueryOp]QueryEventHandler
	DefaultEventHandler      QueryEventHandler
	CloudEventHandlers       map[common.QueryOp]QueryCloudEventHandler
	DefaultCloudEventHandler QueryCloudEventHandler
}

// ConnectNatsForService connects to Nats
func ConnectNatsForService(config *NatsConfig, eventHandlerMappings []QueryEventHandlerMapping) (*NatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectNatsForService",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	options := []nats.Option{}

	if config.MaxReconnects >= 0 {
		options = append(options, nats.MaxReconnects(config.MaxReconnects))
	}

	if config.ReconnectWait >= 0 {
		reconnectWait := time.Duration(config.ReconnectWait) * time.Second
		options = append(options, nats.ReconnectWait(reconnectWait))
	}

	if len(config.WildcardSubject) == 0 {
		err := fmt.Errorf("failed to subscribe an empty subject")
		logger.Error(err)
		return nil, err
	}

	// Connect to Nats
	nc, err := nats.Connect(config.URL, options...)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	natsConn := &NatsConnection{
		Config:                   config,
		Connection:               nc,
		Subscription:             nil,
		HandlerLock:              sync.Mutex{},
		EventHandlers:            map[common.QueryOp]QueryEventHandler{},
		DefaultEventHandler:      nil,
		CloudEventHandlers:       map[common.QueryOp]QueryCloudEventHandler{},
		DefaultCloudEventHandler: nil,
	}

	// Add a handler
	handler := func(msg *nats.Msg) {
		ce, err := ConvertNats(msg)
		if err != nil {
			logger.Error(err)
		} else {
			// handle events
			response, err := natsConn.callEventHandler(&ce)
			if err != nil {
				logger.Error(err)
			} else {
				err = msg.Respond(response)
				if err != nil {
					logger.Error(err)
				}
			}
		}
	}

	// lock handlers
	natsConn.HandlerLock.Lock()

	// Add handlers before subscribe
	for _, eventHandlerMapping := range eventHandlerMappings {
		if len(eventHandlerMapping.Subject) > 0 {
			if eventHandlerMapping.EventHandler != nil {
				natsConn.EventHandlers[common.QueryOp(eventHandlerMapping.Subject)] = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				natsConn.CloudEventHandlers[common.QueryOp(eventHandlerMapping.Subject)] = eventHandlerMapping.CloudEventHandler
			}
		} else {
			// default
			if eventHandlerMapping.EventHandler != nil {
				natsConn.DefaultEventHandler = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				natsConn.DefaultCloudEventHandler = eventHandlerMapping.CloudEventHandler
			}
		}
	}

	natsConn.HandlerLock.Unlock()

	// use QueueSubscribe API
	subscription, err := nc.QueueSubscribe(config.WildcardSubject, config.QueueGroup, handler)
	if err != nil {
		logger.Error(err)
		nc.Close()
		return nil, err
	}

	natsConn.Subscription = subscription
	logger.Tracef("established a connection to %s", config.URL)

	return natsConn, nil
}

// ConnectNatsForServiceClient connects to Nats for service clients who send events
func ConnectNatsForServiceClient(config *NatsConfig) (*NatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectNatsForServiceClient",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	options := []nats.Option{}

	if config.MaxReconnects >= 0 {
		options = append(options, nats.MaxReconnects(config.MaxReconnects))
	}

	if config.ReconnectWait >= 0 {
		reconnectWait := time.Duration(config.ReconnectWait) * time.Second
		options = append(options, nats.ReconnectWait(reconnectWait))
	}

	// Connect to Nats
	nc, err := nats.Connect(config.URL, options...)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	natsConn := &NatsConnection{
		Config:                   config,
		Connection:               nc,
		Subscription:             nil,
		HandlerLock:              sync.Mutex{},
		EventHandlers:            map[common.QueryOp]QueryEventHandler{},
		DefaultEventHandler:      nil,
		CloudEventHandlers:       map[common.QueryOp]QueryCloudEventHandler{},
		DefaultCloudEventHandler: nil,
	}

	logger.Tracef("established a connection to %s", config.URL)

	return natsConn, nil
}

// Disconnect disconnects Nats connection
func (conn *NatsConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.Config.URL)

	if conn.Connection == nil {
		err := fmt.Errorf("connection is not established")
		logger.Error(err)
		return err
	}

	if conn.Subscription != nil {
		conn.Subscription.Unsubscribe()
	}

	if conn.Connection.IsConnected() {
		conn.Connection.Close()
	}

	logger.Tracef("disconnected from %s", conn.Config.URL)

	// clear
	conn.Connection = nil
	conn.Subscription = nil

	conn.HandlerLock.Lock()
	conn.EventHandlers = map[common.QueryOp]QueryEventHandler{}
	conn.DefaultEventHandler = nil
	conn.CloudEventHandlers = map[common.QueryOp]QueryCloudEventHandler{}
	conn.DefaultCloudEventHandler = nil
	conn.HandlerLock.Unlock()
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives subject and JSON data of an event, and returns data object (not required to JSONfy)
func (conn *NatsConnection) AddEventHandler(subject common.QueryOp, eventHandler QueryEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.EventHandlers[common.QueryOp(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddDefaultEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives subject and JSON data of an event, and returns data object (not required to JSONfy)
func (conn *NatsConnection) AddDefaultEventHandler(eventHandler QueryEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.AddDefaultEventHandler",
	})

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives a cloudevent of an event, and returns a cloudevent
func (conn *NatsConnection) AddCloudEventHandler(subject common.QueryOp, eventHandler QueryCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.AddCloudEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.CloudEventHandlers[common.QueryOp(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddDefaultCloudEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives a cloudevent of an event, and returns a cloudevent
func (conn *NatsConnection) AddDefaultCloudEventHandler(eventHandler QueryCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.AddDefaultCloudEventHandler",
	})

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultCloudEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// Request publishes Nats event
// automatically wraps the data with cloud objects, and pills when it returns
func (conn *NatsConnection) Request(ctx context.Context, subject common.QueryOp, data interface{}) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.Request",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.Config.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	responseMsg, err := conn.Connection.RequestWithContext(ctx, ce.Type(), eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	responseCE, err := ConvertNats(responseMsg)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event from response message")
		return nil, err
	}

	return responseCE.Data(), nil
}

// RequestWithTransactionID publishes Nats event
// transactionID will be passed along to the CreateCloudEvent if given
// automatically wraps the data with cloud objects, and pills when it returns
func (conn *NatsConnection) RequestWithTransactionID(ctx context.Context, subject common.QueryOp, data interface{}, transactionID common.TransactionID) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.RequestWithTransactionID",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.Config.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	responseMsg, err := conn.Connection.RequestWithContext(ctx, ce.Type(), eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	responseCE, err := ConvertNats(responseMsg)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event from response message")
		return nil, err
	}

	return responseCE.Data(), nil
}

// RequestCloudEvent publishes Nats event, request and response via cloud event objects
func (conn *NatsConnection) RequestCloudEvent(ctx context.Context, ce *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.RequestCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())
	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	responseMsg, err := conn.Connection.RequestWithContext(ctx, ce.Type(), eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg.Data, nil
}

// calls a event handler for an event and returns response
func (conn *NatsConnection) callEventHandler(event *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.callEventHandler",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("handling an event %s", event.Type())

	conn.HandlerLock.Lock()
	defer conn.HandlerLock.Unlock()

	// call handler
	if handler, ok := conn.EventHandlers[common.QueryOp(event.Type())]; ok {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		response, err := handler(common.QueryOp(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}

		// convert to CE
		ce, err := CreateCloudEventWithTransactionID(response, event.Type(), conn.Config.ClientID, transactionID)
		if err != nil {
			logger.WithError(err).Errorf("failed to convert response to cloud event")
			return nil, err
		}

		eventJSON, err := ce.MarshalJSON()
		if err != nil {
			logger.WithError(err).Errorf("failed to marshal cloud event to JSON")
			return nil, err
		}

		return eventJSON, nil
	} else if handler, ok := conn.CloudEventHandlers[common.QueryOp(event.Type())]; ok {
		// has the handler for the event
		response, err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}
		return response, nil
	} else if conn.DefaultEventHandler != nil {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		response, err := conn.DefaultEventHandler(common.QueryOp(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}

		// convert to CE
		ce, err := CreateCloudEventWithTransactionID(response, event.Type(), conn.Config.ClientID, transactionID)
		if err != nil {
			logger.WithError(err).Errorf("failed to convert response to cloud event")
			return nil, err
		}

		eventJSON, err := ce.MarshalJSON()
		if err != nil {
			logger.WithError(err).Errorf("failed to marshal cloud event to JSON")
			return nil, err
		}

		return eventJSON, nil
	} else if conn.DefaultCloudEventHandler != nil {
		// has the handler for the event
		response, err := conn.DefaultCloudEventHandler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}
		return response, nil
	} else {
		err := fmt.Errorf("failed to find an event handler for a type %s", event.Type())
		logger.Error(err)
		return nil, err
	}
}
