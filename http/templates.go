package http

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// TemplateType represents a template type returned by the HTTP API.
type TemplateType struct {
	Name          string                         `json:"name"`
	Formats       []service.TemplateFormat       `json:"formats,omitempty"`        // e.g., {'yaml', 'json'}
	Engine        service.TemplateEngine         `json:"engine"`                   // e.g., 'terraform'
	ProviderTypes []service.TemplateProviderType `json:"provider_types,omitempty"` // e.g., {'openstack', 'aws', `kubernetes`}
}

// Template represents a template returned by the HTTP API.
type Template struct {
	ID          common.ID                `json:"id"`
	Owner       string                   `json:"owner"`
	Name        string                   `json:"name"`
	Description string                   `json:"description"`
	Public      bool                     `json:"public"`
	Source      service.TemplateSource   `json:"source"`
	Metadata    service.TemplateMetadata `json:"metadata"`
	CreatedAt   time.Time                `json:"created_at"`
	UpdatedAt   time.Time                `json:"updated_at"`

	CredentialID string `json:"credential_id,omitempty"` // for import and sync
	Sync         bool   `json:"sync,omitempty"`          // indicate sync
}
